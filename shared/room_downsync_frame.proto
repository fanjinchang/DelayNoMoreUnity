syntax = "proto3";

package shared;

// Providing "csharp_namespace" to force generating lowercase namespace, reference https://protobuf.dev/programming-guides/proto3/#packages 
option csharp_namespace = "shared";

enum BulletState {
	StartUp = 0;
	Active = 1;
	Exploding = 2;
}

enum CharacterState {
	Idle1 = 0;
	Walking = 1;
	TurnAround = 2;
	Atked1 = 3;
	InAirIdle1NoJump = 4;
	InAirIdle1ByJump = 5;
	InAirIdle1ByWallJump = 6;
	InAirAtked1 = 7;

	BlownUp1 = 8;
	LayDown1 = 9;
	GetUp1 = 10;
	Dying = 11;

	Dashing = 12;
	Sliding = 13;

	OnWallIdle1 = 128; 
    
	CrouchIdle1 = 256; 
	CrouchAtk1 = 257; 
	CrouchWalking = 258; 
	CrouchAtked1 = 259;

	Atk1 = 1024;
	Atk2 = 1025;
	Atk3 = 1026;
	Atk4 = 1027;
	Atk5 = 1028;

	InAirAtk1 = 2048;
	WalkingAtk1 = 2049;

	OnWallAtk1 = 3072; 
}

enum TrapState {
	TIdle = 0;
	TWalking = 1;
	TAtk1 = 2;
	TAtked1 = 3;
	TDestroyed = 4;
}

enum TriggerState {
	TReady = 0;
	TCoolingDown = 1;
}
 
message CharacterDownsync {
	int32 id = 1;
	int32 virtualGridX = 2;
	int32 virtualGridY = 3;
	int32 dirX = 4;
	int32 dirY = 5;   
	int32 velX = 6;
	int32 velY = 7; // "velX" and "velY" is used to record the accumulated effect by inertia and accelerations (including gravity)
	int32 speed = 8; // this is the instantaneous scalar attribute of a character, different from but will be accounted in "velX" and "velY"
	int32 speciesId = 9; // For evolution during battle
	int32 joinIndex = 10;
	
	int32 framesToRecover = 11;
	int32 framesCapturedByInertia = 12; 

	bool capturedByPatrolCue = 13;
	int32 framesInPatrolCue = 14;

	int32 beatsCnt = 15;
	int32 beatenCnt = 16;

	int32 hp = 17;
	int32 maxHp = 18;
	int32 mp = 19;
	int32 maxMp = 20;

	CharacterState characterState = 22;
	int32 framesInChState = 23; // number of frames elapsed in the current character state   
	bool inAir = 24; // by design a standalone field only inferred by the collision result of "Step" instead of "characterState", because we need check the transition for "characterState" from this field, i.e. "inAir (prev -> curr)"  
    bool jumpTriggered = 25;

	bool onWall = 26; // like "inAir", it’s by design a standalone field only inferred by the collision result of "Step" instead of "characterState", because we need check the transition for "characterState" from this field, i.e. "onWall (prev -> curr)"
	int32 onWallNormX = 27;
	int32 onWallNormY = 28;

	bool omitGravity = 29;
	bool omitSoftPushback = 30;

	int32 waivingPatrolCueId = 31;
	bool waivingSpontaneousPatrol = 32;

	int32 revivalVirtualGridX = 33;
	int32 revivalVirtualGridY = 34;
	int32 revivalDirX = 35;
	int32 revivalDirY = 36;

	bool onSlope = 37;

	int32 frictionVelX = 38; // Mostly from a dynamic trap
    bool forcedCrouching = 39; // like "inAir", it’s by design a standalone field only inferred by the collision result of "Step" instead of "characterState", because we need check the transition for "characterState" from this field, i.e. "onWall (prev -> curr)"
        
	bool slipJumpTriggered = 40;
	bool primarilyOnSlippableHardPushback = 41; // like "inAir", it’s by design a standalone field only inferred by the calc result of "Battle.Step" instead of "characterState"
	bool prevWasCrouching = 42;
	bool newBirth = 43;
	int32 lowerPartFramesInChState = 44;

    bool jumpStarted = 45;
    int32 framesToStartJump = 46;

	bool repelSoftPushback = 47;

	string name = 997;
	string displayName = 998;
	string avatar = 999;

	int32 activeSkillId = 1024;
	int32 activeSkillHit = 1025;
	int32 framesInvinsible = 1026;

    // Kindly note that "collisionTypeMask" is NOT GRANULAR ENOUGH for inter-character collision configuration, e.g. same team omission, thus we'd still need the fields "bulletTeamId" and "chCollisionTeamId" here
	int32 bulletTeamId = 1027;
	int32 chCollisionTeamId = 1028;
	uint64 collisionTypeMask = 1029;

    repeated Buff buffList = 1030; 
    repeated Debuff debuffList = 1031; 

	Inventory inventory = 1032;
}

message InputFrameDecoded {
	int32 dx = 1;
	int32 dy = 2;
	int32 btnALevel = 3;
	int32 btnBLevel = 4;
	int32 btnCLevel = 5;
	int32 btnDLevel = 6;
}

message InputFrameUpsync {
	int32 inputFrameId = 1;
	uint64 encoded = 2;
	int32 joinIndex = 3;
}

message InputFrameDownsync {
	int32 inputFrameId = 1;
	repeated uint64 inputList = 2; // Indexed by "joinIndex", we try to compress the "single player input" into 1 word (64-bit for 64-bit Golang runtime) because atomic compare-and-swap only works on 1 word. Although CAS on custom struct is possible in C# https://learn.microsoft.com/en-us/dotnet/api/system.threading.interlocked?view=netstandard-2.1, using a single word is still faster whenever possible. 
	uint64 confirmedList = 3; // Indexed by "joinIndex", same compression concern as above
}

message WsReq {
	int32 playerId = 1;
	int32 act = 2;
	int32 joinIndex = 3;
	int32 ackingFrameId = 4;
	int32 ackingInputFrameId = 5;
	int32 authKey = 6;
	repeated InputFrameUpsync inputFrameUpsyncBatch = 7;
}

message InputBufferSnapshot {
	int32 refRenderFrameId = 1;
	uint64 unconfirmedMask = 2;
	repeated InputFrameDownsync toSendInputFrameDownsyncs = 3;
	bool shouldForceResync = 4;
	int32 peerJoinIndex = 5;
}

message PatrolCue {
	int32  id = 1;
    uint64 flAct = 2; // "input when colliding from the left side"
    uint64 frAct = 3; // "input when colliding from the right side"
	uint64 flCaptureFrames = 4;
	uint64 frCaptureFrames = 5;

    uint64 fdAct = 6; // "input when colliding from the down side"
    uint64 fuAct = 7; // "input when colliding from the up side"
	uint64 fdCaptureFrames = 8;
	uint64 fuCaptureFrames = 9;

	uint64 collisionTypeMask = 1029;
}

message VisionBox {
    // all values are relative to (CharacterDownsync.dirX=+2, CharacterDownsync.virtualGridX, CharacterDownsync.virtualGridY) 
    int32 bottomLeftX = 1; 
    int32 bottomLeftY = 2; 
    int32 width = 3; 
    int32 height = 4; 
}

enum BulletType {
	Undetermined = 0;
	Melee = 1;
	Fireball = 2;
}

enum MultiHitType {
	/**
	[WARN] Multihit of a fireball (determined in turn by the "BulletType bType" field) is more difficult to handle than that of melee, because we have to count from the fireball's first hit; the situation becomes even more complicated when a multihit fireball is in a crowd -- remains to be designed

	Types of fireball multihit-singlebullet
	a. References the emission frame
	b. References the first hit only when actually hit
	c. References the first hit even if blocked
	d. References the prev hit only when actually hit
	e. References the prev hit even if blocked
	*/
	None=0;
    FromEmission = 1;
    FromFirstHitActual = 2;
    FromFirstHitAnyway = 3;
    FromPrevHitActual = 4;
    FromPrevHitAnyway = 5;
}

enum SkillTriggerType {
	RisingEdge = 0;
	FallingEdge = 1;
}

enum VfxDurationType {
    OneOff = 0;
    Repeating = 1;
}

enum VfxMotionType {
    NoMotion = 0;
    Dropped = 1;
    Tracing = 2;
}

message VfxConfig {
    int32 speciesId = 1;
    VfxMotionType motionType = 2;
    VfxDurationType durationType = 3;
	bool onCharacter = 4;
	bool onBullet = 5;
}

message BulletConfig {
	int32 startupFrames = 1;
	int32 cancellableStFrame = 2;
	int32 cancellableEdFrame = 3;
	int32 activeFrames = 4;

	int32 hitStunFrames = 5;
	int32 blockStunFrames = 6;
	int32 pushbackVelX = 7;
	int32 pushbackVelY = 8;
	int32 damage = 9;

	int32 selfLockVelX = 10;
	int32 selfLockVelY = 11;

	int32 hitboxOffsetX = 12;
	int32 hitboxOffsetY = 13;
	int32 hitboxSizeX = 14;
	int32 hitboxSizeY = 15;

	bool blowUp = 16;

	int32 speciesId = 17; // For fireball, this SpeciesId specifies both the active animation and the explosion animation (if "explosionSpeciesId" not explicitly assigned), for melee it specifies the explosion animation
	int32 explosionFrames = 18;

	BulletType bType = 19;
	MultiHitType mhType = 20;
    uint32 simultaneousMultiHitCnt = 21; // marks how many simultaneous bullets are left, default to 0 indicating that the current bullet is the only one to fire 

	int32 speed = 22; // For speed varying bullets, this is the initial speed
	int32 dirX = 23; // the initial direction indicator for X-axis
	int32 dirY = 24; // the initial direction indicator for Y-axis

	map<int32, int32> cancelTransit = 25; // patternId -> skillId
    int32 repeatQuota = 26; // for long lasting bullet, applicable for "mhType == (FromPrevHitActual | FromPrevHitAnyway)"

    int32 activeVfxSpeciesId = 27; // currently only support 1 vfx config when active
    int32 explosionVfxSpeciesId = 28; // currently only support 1 vfx config when exploding

    bool allowsWalking = 29;
    bool allowsCrouching = 30;
    int32 hardness = 31;
    int32 startupInvinsibleFrames = 32;
    int32 hitInvinsibleFrames = 33;
	int32 explosionSpeciesId = 34;

	int32 perpAccMag = 35; // acceleration perpendicular to instantaneous velocity per frame

    bool delaySelfVelToActive = 36; // if true, "selfLockVelX" & "selfLockVelY" would only be applied when bullet is active 

	int32 speedIfNotHit = 37;
    bool omitSoftPushback = 38; // Only used for melee bullets

    int32 hitboxSizeIncX = 39; // Change of hitboxSizeX per frame
    int32 hitboxSizeIncY = 40; // Change of hitboxSizeY per frame

    bool remainUponHit = 41; 

	uint64 collisionTypeMask = 1029;
    bool takesGravity = 1030; // e.g. a bomb

	// Sfx configs
	string characterEmitSfxName = 2048;
	string fireballEmitSfxName = 2049;
	string explosionSfxName = 2050;
	string activeSfxName = 2051;
}

message BulletBattleAttr {
	int32 bulletLocalId = 1;
	int32 originatedRenderFrameId = 2; // Copied from the first bullet for all subsequent bullets
	int32 offenderJoinIndex = 3; // Copied to favor collision handling of the dispatched bullet
	int32 teamId = 4;
	int32 activeSkillHit = 5; // for the convenience of selecting the correct next bullet for multihit fireball (where the offender's "activeSkillHit" might've already been reset)
	int32 skillId = 6; // for the convenience of selecting the correct next bullet for multihit fireball (where the offender's "activeSkillId" might've already been reset); also for reverse lookup in multihit transition, in the most extreme case, a same "BulletConfig" instance is to be shared across different "Skill"s, thus we put "skillId" in "BulletBattleAttr" instead.
}

message Bullet {
	// Jargon reference https://www.thegamer.com/fighting-games-frame-data-explained/
	// ALL lengths are in world coordinate
	BulletState blState = 1;
	int32 framesInBlState = 2;
	BulletBattleAttr battleAttr = 3;
	BulletConfig config = 4;
	int32 repeatQuotaLeft = 5;

	int32 virtualGridX = 999;
	int32 virtualGridY = 1000;
	int32 dirX = 1001;
	int32 dirY = 1002;
	int32 velX = 1003;
	int32 velY = 1004;
}

message TrapConfig {
    int32 speciesId = 1;
	int32 explosionSpeciesId = 2; 
	int32 explosionFrames = 3;
	bool blowUp = 4;

    int32 damage = 5;
	int32 hitStunFrames = 6;
	int32 hitInvinsibleFrames = 7;
    bool destroyUponHit = 8; // for most traps and long lasting melee bullet

    int32 hardness = 9;

	// TODO: Instead of damage, a trap should also be able to provide pushbacks like a "trampoline" in only a limited "framesToRecover", possible started by a Trigger.

	string speciesName = 1024;
}

message TrapConfigFromTiled {
    /*
    By the time of writing, I am not yet very confident on what should be completely hardcoded constant in C# and what should be flexibly configured from Tiled Editor. 

    The simple mindset ruling here 
    - we couldn't make configurable items infinitely granular, at least not at this early stage of development
    - for a "completely static trap" (not moving & not changing appearance even if destroyed), both its appearance and hitbox can be drawn in Tiled Editor
    - for a "dynamic trap" (either moving or changing appearance anyway), both its appearance and hitbox can be drawn in Tiled TSX Editor, put onto an Object Layer in Tiled Editor for preview -- however the object on the Object Layer will be removed immediately and replaced by a Trap object in "RoomDownsyncFrame.trapsArr" for dynamic management    
    */
    int32 speciesId = 1; // Will be used for the "dynamic trap" to locate its Prefab for animating, and used for all types of traps to locate their explosion animations.
    int32 quota = 2;

	int32 speed = 3; // For speed varying bullets, this is the initial speed
	int32 dirX = 4; // the initial direction indicator for X-axis
	int32 dirY = 5; // the initial direction indicator for Y-axis

	bool prohibitsWallGrabbing = 6;
}

message TrapColliderAttr {
    // In the case of a trap, it might own multiple hitboxes and each hitbox is drawn in Tiled and assigned after parsing the tmx file, thus not put in static config.
	int32 hitboxOffsetX = 1;
	int32 hitboxOffsetY = 2;
	int32 hitboxSizeX = 3;
	int32 hitboxSizeY = 4;
    bool providesHardPushback = 5;
    bool providesDamage = 6;
    bool providesEscape = 7;
	bool providesSlipJump = 8;
	bool forcesCrouching = 9;

	int32 trapLocalId = 1024; // ref-back upon collision, for damage handling, hardPushback moving and effPushbacks recording (if the trap provides softPushback)
	uint64 collisionTypeMask = 1029;
}

/*
What distinguished "Trap" and "Npc(typed CharacterDownsync)" is that a "Trap" often has much simpler skillset, vision/patrol reaction -- yet more complicated boundaries and hard/soft pushback config.
*/
message Trap {
    int32 trapLocalId = 1; // [WARNING] Used to look up proper "List<TrapColliderAttr>" which is configured in Tiled Editor and remained static during the whole battle!
	TrapConfig config = 2;
	TrapConfigFromTiled configFromTiled = 3;

	TrapState trapState = 4;
	int32 framesInTrapState = 5;
	/*
	[WARNING] Here "virtualGridX" and "virtualGridY" denotes the point in virtual grid for local (0, 0) of a trap, then each "colliderAttr" carries "hitboxOffsetX", "hitboxOffsetY", "hitboxSizeX" and "hitboxSizeY" also in virtual grid!

	It's bit embarrassing that in Tiled Editor
	- if we draw a rectangle in an Object Layer by the rectangle tool, its local (0, 0) is the bottom-left corner, but
	- if we put an image object in an Object Layer, its local (0, 0) is the upper-left corner

	such a frustrating default so we'll use a uniformed local (0, 0) -- the center of rectangle.
	*/
	bool isCompletelyStatic = 6;
	bool capturedByPatrolCue = 7;
	int32 framesInPatrolCue = 8;
	int32 waivingPatrolCueId = 9;
	bool waivingSpontaneousPatrol = 10;

    int32 triggerTrackingId = 11;

	int32 virtualGridX = 999; 
	int32 virtualGridY = 1000;
	int32 dirX = 1001;
	int32 dirY = 1002;
	int32 velX = 1003;
	int32 velY = 1004;
}

message TriggerConfig {
	int32 speciesId = 1;
    string speciesName = 2; 
    uint64 triggerMask = 3;
    /*
    By now "TRIGGER_MASK_BY_CYCLIC_TIMER" couldn't coexist with others, and here's how the major params work  

    :repeats "quota" times
                      |<-------------------------------------- recoveryFrames ----------------------------->|<-----------------recoveryFrames ---------->|.......
                      :repeats "subCycleQuota" times                                                        :repeats "subCycleQuota" times
    |<-delayedFrames->|<-subCycleTriggerFrames->|<-subCycleTriggerFrames->|<-subCycleTriggerFrames->|       |.......
    |TReady           |TCoolingDown             |TCoolingDown(replay)     |TCoolingDown(replay)     |TReady |.......
    */
	uint64 collisionTypeMask = 4; // of this trigger
}

message CharacterSpawnerConfig {
    int32 cutoffRdfFrameId = 1;
    repeated int32 speciesIdList = 2; // the k-th subcycle tick corresponds to speciesIdList[k]   
}

message TriggerConfigFromTiled {
	int32 speciesId = 1;
    int32 delayedFrames = 2;  
    int32 recoveryFrames = 3;
    repeated int32 trackingIdList = 4;

    int32 initVelX = 5;
    int32 initVelY = 6;

	int32 chCollisionTeamId = 7;  // of this trigger

    int32 subCycleTriggerFrames = 8;
    int32 subCycleQuota = 9;

    repeated CharacterSpawnerConfig characterSpawnerTimeSeq = 10; // MUST Be configured by increasing "cutoffRdfFrameId"!
}

message TriggerColliderAttr {
	int32 triggerLocalId = 1; // ref-back upon collision, most (actually by now ALL) triggers are static, we use this "TriggerColliderAttr,triggerLocalId" to save the need of dynamically inserting and deleting colliders of each trigger while still being able to be rollback-compatible for "trigger.framesToFire & trigger.framesToRecover"!
}

message Trigger {
    int32 triggerLocalId = 1;
    int32 framesToFire = 2; 
    int32 framesToRecover = 3; // if TRIGGER_MASK_BY_CYCLIC_TIMER, resets whenever "0 < subCycleQuotaLeft" and itself becomes 0
    int32 quota = 4; // the initial value is from static config, but decreases upon use 
	int32 bulletTeamId = 5; 

    TriggerConfig config = 6; // static config, unchanged during battle
    TriggerConfigFromTiled configFromTiled = 7; // static config, unchanged during battle

    int32 subCycleQuotaLeft = 8; // resets whenever "framesToRecover" becomes 0 
   
    TriggerState state = 9; 
    int32 framesInState = 10; 

	int32 virtualGridX = 11;
	int32 virtualGridY = 12;
}

message PeerUdpAddr {
	string ip = 1;
	int32 port = 2;
	int32 authKey = 3;
}

message BattleColliderInfo {
	string stageName = 1;

	int32 boundRoomId = 2;
	int64 battleDurationFrames = 3;
	int32 inputFrameUpsyncDelayTolerance = 4;
	int32 maxChasingRenderFramesPerUpdate = 5;
	double rollbackEstimatedDtMillis = 6;
	int64 rollbackEstimatedDtNanos = 7;

	int32 renderBufferSize = 8;
	int32 collisionMinStep = 9;
	int32 boundRoomCapacity = 10;
	int32 preallocNpcCapacity = 11;
	int32 preallocBulletCapacity = 12;

	PeerUdpAddr battleUdpTunnel = 1024;
	bool frameLogEnabled = 1025;
}

message RoomDownsyncFrame {
	int32 id = 1;
	repeated CharacterDownsync playersArr = 2;
	int64 countdownNanos = 3;
	repeated Bullet bullets = 4;  

	repeated CharacterDownsync npcsArr = 5;
	repeated Trap trapsArr = 6;
	repeated Trigger triggersArr = 7;

	uint64 backendUnconfirmedMask = 1024; // Indexed by "joinIndex", same compression concern as stated in InputFrameDownsync
	bool shouldForceResync = 1025;
	repeated int32 speciesIdList = 1026;

	int32 bulletLocalIdCounter = 1027;
	int32 npcLocalIdCounter = 1028;

	int32 participantChangeId = 1029;

	repeated PeerUdpAddr peerUdpAddrList = 2048;
}

message WsResp {
	int32 ret = 1;
	int32 act = 2;
	RoomDownsyncFrame rdf = 3;
	repeated InputFrameDownsync inputFrameDownsyncBatch = 4;
	BattleColliderInfo bciFrame = 5;
	int32 peerJoinIndex = 6;
	string errMsg = 7;
}

message Skill {
	int32 battleLocalId = 1;
	int32 recoveryFrames = 2;
	int32 recoveryFramesOnBlock = 3; 
	int32 recoveryFramesOnHit = 4; 
	SkillTriggerType triggerType = 5;
	CharacterState boundChState = 6;
	int32 mpDelta = 7; // Usually used as "mp consumption"

	repeated BulletConfig hits = 1024; // Hits within a "Skill" are automatically triggered
}

message FrameLog {
    RoomDownsyncFrame rdf = 1;
    InputFrameDownsync actuallyUsedIdf = 2;
}

message BattleResult {
    int32 winnerJoinIndex = 1;
}

enum BuffStockType {
    NoneBf = 0;
	Timed = 1; // Would be calculated in number of renderFrames, not actual time in seconds or milliseconds
	Quota = 2;
}

enum DebuffType {
    NoneDbf = 0;
    SpeedDownOnly = 1;
    ColdSpeedDown = 2;
    PositionLockedOnly = 3;
    FrozenPositionLocked = 4;
}

message DebuffConfig {
    int32 speciesId = 1;
    BuffStockType stockType = 2;
    int32 stock = 3;
    DebuffType type = 4;
    int32 arrIdx = 5;
}

message BuffConfig {
    int32 speciesId = 1;
    BuffStockType stockType = 2;
    int32 stock = 3;
    int32 speedDelta = 4;
    int32 hitboxSizeXDelta = 5;
    int32 hitboxSizeYDelta = 6;
    /* 
    It's not trivial to handle damage after the following sequence
    1. Pick up damageDelta=+10 for 600 frames
    2. Pick up damageDeltaByPercentage=+10% for 600 frames
    3. Buff "damageDelta=+10" expires 
    4. Buff "damageDeltaByPercentage=+10%" expires

    Pick a deterministic way :)
    */
    int32 damageDelta = 7;
    int32 damageDeltaByPercentage = 8;
    int32 characterHardnessDelta = 9;
    int32 meleeHardnessDelta = 10;
    int32 fireballHardnessDelta = 11;
    bool invinsible = 12;
    int32 xformChSpeciesId = 13;
    repeated DebuffConfig associatedDebuffs = 1024; // There's a reason why type "DebuffConfig" instead of just the speciesId is used here, as "BuffConfig" itself is already seeked via heap-RAM dictionary look from "Buff.speciesId", we'd like to reduce the count of heap-RAM access when stepping battle dynamics  

    int32 characterVfxSpeciesId = 2048;
}

message Buff {
    int32 speciesId = 1; // A redundancy of "buffConfig" for easy termination detection
    int32 stock = 2;
    BuffConfig buffConfig = 3; // Same type concern as "BuffConfig.associatedDebuffs". 
    int32 originatedRenderFrameId = 4;
    int32 origChSpeciesId = 5;
}

message Debuff {
    int32 speciesId = 1; // A redundancy of "debuffConfig" for easy termination detection 
    int32 stock = 2;
    DebuffConfig debuffConfig = 3; // Same type concern as "BuffConfig.associatedDebuffs". 
}

enum InventorySlotStockType {
	NoneIv = 0;
	TimedIv = 1; // Reusable after cooldown
	QuotaIv = 2;
	TimedMagazineIv = 3; // Has certain quotas before required to cooldown
}

message InventorySlot {
	InventorySlotStockType stockType = 1;
	int32 quota = 2; 
	int32 framesToRecover = 3; 
    int32 defaultQuota = 4;
    int32 defaultFramesToRecover = 5;
	BuffConfig buffConfig = 1024; // Same type concern as "BuffConfig.associatedDebuffs".
}

message Inventory {
	repeated InventorySlot slots = 1;
}
