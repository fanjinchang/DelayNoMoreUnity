﻿using UnityEngine;
using shared;
using static shared.Battle;
using System;
using System.Collections.Generic;
using Pbc = Google.Protobuf.Collections;
using SuperTiled2Unity;
using System.Collections;
using DG.Tweening;
using UnityEngine.XR;

public abstract class AbstractMapController : MonoBehaviour {
    protected int roomCapacity;
    protected int maxTouchingCellsCnt;
    protected int battleDurationFrames;

    protected int preallocNpcCapacity = DEFAULT_PREALLOC_NPC_CAPACITY;
    protected int preallocBulletCapacity = DEFAULT_PREALLOC_BULLET_CAPACITY;
    protected int preallocTrapCapacity = DEFAULT_PREALLOC_TRAP_CAPACITY;
    protected int preallocTriggerCapacity = DEFAULT_PREALLOC_TRIGGER_CAPACITY;

    protected int playerRdfId; // After battle started, always increments monotonically (even upon reconnection)
    protected int renderFrameIdLagTolerance;
    protected int lastAllConfirmedInputFrameId;
    protected int lastUpsyncInputFrameId;

    protected int chaserRenderFrameId; // at any moment, "chaserRenderFrameId <= renderFrameId", but "chaserRenderFrameId" would fluctuate according to "onInputFrameDownsyncBatch"
    protected int maxChasingRenderFramesPerUpdate;
    protected int renderBufferSize;
    public GameObject inplaceHpBarPrefab;
    public GameObject fireballPrefab;
    public GameObject errStackLogPanelPrefab;
    public GameObject teamRibbonPrefab;
    public GameObject sfxSourcePrefab;
    protected GameObject errStackLogPanelObj;
    protected GameObject underlyingMap;
    public Canvas canvas;

    protected int[] lastIndividuallyConfirmedInputFrameId;
    protected ulong[] lastIndividuallyConfirmedInputList;
    protected CharacterDownsync selfPlayerInfo = null;
    protected FrameRingBuffer<RoomDownsyncFrame> renderBuffer = null;
    protected FrameRingBuffer<RdfPushbackFrameLog> pushbackFrameLogBuffer = null;
    protected FrameRingBuffer<InputFrameDownsync> inputBuffer = null;
    protected FrameRingBuffer<shared.Collider> residueCollided = null;

    protected ulong[] prefabbedInputListHolder;
    protected GameObject[] playerGameObjs;
    protected List<GameObject> dynamicTrapGameObjs;
    protected Dictionary<int, GameObject> triggerGameObjs; // They actually don't move

    protected long battleState;
    protected int spaceOffsetX;
    protected int spaceOffsetY;
    protected float effectivelyInfinitelyFar;

    protected RoomDownsyncFrame historyRdfHolder;
    protected shared.Collision collisionHolder;
    protected SatResult overlapResult, primaryOverlapResult;
    protected Dictionary<int, BattleResult> unconfirmedBattleResult;
    protected BattleResult confirmedBattleResult;
    protected Vector[] effPushbacks;
    protected Vector[][] hardPushbackNormsArr;
    protected Vector[] softPushbacks;
    protected bool softPushbackEnabled;
    protected shared.Collider[] dynamicRectangleColliders;
    protected shared.Collider[] staticColliders;
    protected InputFrameDecoded decodedInputHolder, prevDecodedInputHolder;
    protected CollisionSpace collisionSys;

    public GameObject linePrefab;
    protected KvPriorityQueue<string, FireballAnimController> cachedFireballs;
    protected Vector3[] debugDrawPositionsHolder = new Vector3[4]; // Currently only rectangles are drawn
    protected KvPriorityQueue<string, DebugLine> cachedLineRenderers;
    protected Dictionary<int, GameObject> npcSpeciesPrefabDict;
    protected Dictionary<int, KvPriorityQueue<string, CharacterAnimController>> cachedNpcs;
    protected KvPriorityQueue<string, TeamRibbon> cachedTeamRibbons;
    protected KvPriorityQueue<string, InplaceHpBar> cachedHpBars;

    protected bool shouldDetectRealtimeRenderHistoryCorrection = false; // Not recommended to enable in production, it might have some memory performance impact.
    protected bool frameLogEnabled = false;
    protected Dictionary<int, InputFrameDownsync> rdfIdToActuallyUsedInput;
    protected Dictionary<int, List<TrapColliderAttr>> trapLocalIdToColliderAttrs;
    protected Dictionary<int, int> triggerTrackingIdToTrapLocalId;

    protected List<shared.Collider> completelyStaticTrapColliders;

    public CharacterSelectPanel characterSelectPanel;

    protected Dictionary<int, GameObject> vfxSpeciesPrefabDict;
    protected Dictionary<int, KvPriorityQueue<string, VfxNodeController>> cachedVfxNodes; // first layer key is the speciesId, second layer key is the "entityType+entityLocalId" of either a character (i.e. "ch-<joinIndex>") or a bullet (i.e. "bl-<bulletLocalId>")

    protected KvPriorityQueue<string, SFXSource> cachedSfxNodes;
    public AudioSource bgmSource;
    public abstract void onCharacterSelectGoAction(int speciesId);

    protected bool debugDrawingAllocation = false;
    protected bool debugDrawingEnabled = false;

    protected ILoggerBridge _loggerBridge = new LoggerBridgeImpl();

    public SelfBattleHeading selfBattleHeading;

    public GameObject playerLightsPrefab;
    protected PlayerLights selfPlayerLights;

    protected Vector2 teamRibbonOffset = new Vector2(-10f, +6f);
    protected Vector2 inplaceHpBarOffset = new Vector2(-8f, +2f);
    protected float lineRendererZ = +5;
    protected float triggerZ = 0;
    protected float characterZ = 0;
    protected float inplaceHpBarZ = +10;
    protected float fireballZ = -5;
    protected float footstepAttenuationZ = 200.0f;

    private string MATERIAL_REF_THICKNESS = "_Thickness";
    private string MATERIAL_REF_FLASH_INTENSITY = "_FlashIntensity";
    private string MATERIAL_REF_FLASH_COLOR = "_FlashColor";
    private float DAMAGED_FLASH_INTENSITY = 0.4f;
    private float DAMAGED_THICKNESS = 1.5f;
    private float DAMAGED_BLINK_SECONDS_HALF = 0.2f;

    protected KvPriorityQueue<string, TeamRibbon>.ValScore cachedTeamRibbonScore = (x) => x.score;
    protected KvPriorityQueue<string, InplaceHpBar>.ValScore cachedHpBarScore = (x) => x.score;
    protected KvPriorityQueue<string, CharacterAnimController>.ValScore cachedNpcScore = (x) => x.score;
    protected KvPriorityQueue<string, FireballAnimController>.ValScore cachedFireballScore = (x) => x.score;
    protected KvPriorityQueue<string, DebugLine>.ValScore cachedLineScore = (x) => x.score;
    protected KvPriorityQueue<string, VfxNodeController>.ValScore vfxNodeScore = (x) => x.score;
    protected KvPriorityQueue<string, SFXSource>.ValScore sfxNodeScore = (x) => x.score;

    public BattleInputManager iptmgr;

    protected GameObject loadCharacterPrefab(CharacterConfig chConfig) {
        string path = String.Format("Prefabs/{0}", chConfig.SpeciesName);
        return Resources.Load(path) as GameObject;
    }

    protected GameObject loadTrapPrefab(TrapConfig trapConfig) {
        string path = String.Format("Prefabs/{0}", trapConfig.SpeciesName);
        return Resources.Load(path) as GameObject;
    }

    protected GameObject loadTriggerPrefab(TriggerConfig triggerConfig) {
        string path = String.Format("Prefabs/{0}", triggerConfig.SpeciesName);
        return Resources.Load(path) as GameObject;
    }

    public ReadyGo readyGoPanel;
    public SettlementPanel settlementPanel;
    protected Vector3 newPosHolder = new Vector3();
    protected Vector3 newTlPosHolder = new Vector3(), newTrPosHolder = new Vector3(), newBlPosHolder = new Vector3(), newBrPosHolder = new Vector3();
    protected delegate void PostSettlementCallbackT();

    protected PostSettlementCallbackT postSettlementCallback;

    protected void spawnPlayerNode(int joinIndex, int speciesId, float wx, float wy, int bulletTeamId) {
        var characterPrefab = loadCharacterPrefab(characters[speciesId]);
        GameObject newPlayerNode = Instantiate(characterPrefab, new Vector3(wx, wy, characterZ), Quaternion.identity, underlyingMap.transform);
        playerGameObjs[joinIndex - 1] = newPlayerNode;
    }

    protected void spawnDynamicTrapNode(int speciesId, float wx, float wy) {
        var trapPrefab = loadTrapPrefab(trapConfigs[speciesId]);
        GameObject newTrapNode = Instantiate(trapPrefab, new Vector3(wx, wy, triggerZ), Quaternion.identity, underlyingMap.transform);
        dynamicTrapGameObjs.Add(newTrapNode);
    }

    protected void spawnTriggerNode(int triggerLocalId, int speciesId, float wx, float wy) {
        var triggerPrefab = loadTriggerPrefab(triggerConfigs[speciesId]);
        GameObject newTriggerNode = Instantiate(triggerPrefab, new Vector3(wx, wy, triggerZ), Quaternion.identity, underlyingMap.transform);
        triggerGameObjs[triggerLocalId] = newTriggerNode;
    }

    protected (ulong, ulong) getOrPrefabInputFrameUpsync(int inputFrameId, bool canConfirmSelf, ulong[] prefabbedInputList) {
        if (null == selfPlayerInfo) {
            string msg = String.Format("noDelayInputFrameId={0:D} couldn't be generated due to selfPlayerInfo being null", inputFrameId);
            throw new ArgumentException(msg);
        }

        ulong previousSelfInput = 0,
          currSelfInput = 0;
        int joinIndex = selfPlayerInfo.JoinIndex;
        ulong selfJoinIndexMask = (1UL << (joinIndex - 1));
        var (_, existingInputFrame) = inputBuffer.GetByFrameId(inputFrameId);
        var (_, previousInputFrameDownsync) = inputBuffer.GetByFrameId(inputFrameId - 1);
        previousSelfInput = (null == previousInputFrameDownsync ? 0 : previousInputFrameDownsync.InputList[joinIndex - 1]);
        if (
          null != existingInputFrame
          &&
          (true != canConfirmSelf)
        ) {
            return (previousSelfInput, existingInputFrame.InputList[joinIndex - 1]);
        }

        Array.Fill<ulong>(prefabbedInputList, 0);
        for (int k = 0; k < roomCapacity; ++k) {
            if (null != existingInputFrame) {
                // When "null != existingInputFrame", it implies that "true == canConfirmSelf" here, we just have to assign "prefabbedInputList[(joinIndex-1)]" specifically and copy all others
                prefabbedInputList[k] = existingInputFrame.InputList[k];
            } else if (lastIndividuallyConfirmedInputFrameId[k] <= inputFrameId) {
                prefabbedInputList[k] = lastIndividuallyConfirmedInputList[k];
                // Don't predict "btnA & btnB"!
                prefabbedInputList[k] = (prefabbedInputList[k] & 15);
            } else if (null != previousInputFrameDownsync) {
                // When "self.lastIndividuallyConfirmedInputFrameId[k] > inputFrameId", don't use it to predict a historical input!
                prefabbedInputList[k] = previousInputFrameDownsync.InputList[k];
                // Don't predict "btnA & btnB"!
                prefabbedInputList[k] = (prefabbedInputList[k] & 15);
            }
        }

        // [WARNING] Do not blindly use "selfJoinIndexMask" here, as the "actuallyUsedInput for self" couldn't be confirmed while prefabbing, otherwise we'd have confirmed a wrong self input by "_markConfirmationIfApplicable()"!
        ulong initConfirmedList = 0;
        if (null != existingInputFrame) {
            // When "null != existingInputFrame", it implies that "true == canConfirmSelf" here
            initConfirmedList = (existingInputFrame.ConfirmedList | selfJoinIndexMask);
        }
        currSelfInput = iptmgr.GetEncodedInput(); // When "null == existingInputFrame", it'd be safe to say that "GetImmediateEncodedInput()" is for the requested "inputFrameId"
        prefabbedInputList[(joinIndex - 1)] = currSelfInput;
        while (inputBuffer.EdFrameId <= inputFrameId) {
            // Fill the gap
            int gapInputFrameId = inputBuffer.EdFrameId;
            inputBuffer.DryPut();
            var (ok, ifdHolder) = inputBuffer.GetByFrameId(gapInputFrameId);
            if (!ok || null == ifdHolder) {
                throw new ArgumentNullException(String.Format("inputBuffer was not fully pre-allocated for gapInputFrameId={0}! Now inputBuffer StFrameId={1}, EdFrameId={2}, Cnt/N={3}/{4}", gapInputFrameId, inputBuffer.StFrameId, inputBuffer.EdFrameId, inputBuffer.Cnt, inputBuffer.N));
            }

            ifdHolder.InputFrameId = gapInputFrameId;
            for (int k = 0; k < roomCapacity; ++k) {
                ifdHolder.InputList[k] = prefabbedInputList[k];
            }
            ifdHolder.ConfirmedList = initConfirmedList;
        }

        return (previousSelfInput, currSelfInput);
    }

    protected (RoomDownsyncFrame, RoomDownsyncFrame) rollbackAndChase(int renderFrameIdSt, int renderFrameIdEd, CollisionSpace collisionSys, bool isChasing) {
        RoomDownsyncFrame prevLatestRdf = null, latestRdf = null;
        for (int i = renderFrameIdSt; i < renderFrameIdEd; i++) {
            var (ok1, currRdf) = renderBuffer.GetByFrameId(i);
            if (false == ok1 || null == currRdf) {
                throw new ArgumentNullException(String.Format("Couldn't find renderFrame for i={0} to rollback, playerRdfId={1}, might've been interruptted by onRoomDownsyncFrame", i, playerRdfId));
            }
            if (currRdf.Id != i) {
                throw new ArgumentException(String.Format("Corrupted historic rdf for i={0} to rollback, currRdf={1}!", i, currRdf));
            }
            int j = ConvertToDelayedInputFrameId(i);
            var (ok2, delayedInputFrame) = inputBuffer.GetByFrameId(j);
            if (false == ok2 || null == delayedInputFrame) {
                throw new ArgumentNullException(String.Format("Couldn't find delayedInputFrame for j={0} to rollback, playerRdfId={1}", j, playerRdfId));
            }

            bool allowUpdateInputFrameInPlaceUponDynamics = (!isChasing);
            if (allowUpdateInputFrameInPlaceUponDynamics) {
                bool hasInputBeenMutated = UpdateInputFrameInPlaceUponDynamics(j, roomCapacity, delayedInputFrame.ConfirmedList, delayedInputFrame.InputList, lastIndividuallyConfirmedInputFrameId, lastIndividuallyConfirmedInputList, selfPlayerInfo.JoinIndex);
                if (hasInputBeenMutated) {
                    int ii = ConvertToFirstUsedRenderFrameId(j);
                    if (ii < i) {
                        /*
                           [WARNING] 
                           If we don't rollback at this spot, when the mutated "delayedInputFrame.inputList" a.k.a. "inputFrame#j" matches the later downsynced version, rollback WOULDN'T be triggered for the incorrectly rendered "renderFrame#(ii+1)", and it would STAY IN HISTORY FOREVER -- as the history becomes incorrect, EVERY LATEST renderFrame since "inputFrame#j" was mutated would be ALWAYS incorrectly rendering too!

                           The update to chaserRenderFrameId here would NOT impact the current cycle of rollbackAndChase !
                         */
                        _handleIncorrectlyRenderedPrediction(j, false);
                    }
                }
            }

            bool hasIncorrectlyPredictedRenderFrame = false;
            Step(inputBuffer, i, roomCapacity, collisionSys, renderBuffer, ref overlapResult, ref primaryOverlapResult, collisionHolder, effPushbacks, hardPushbackNormsArr, softPushbacks, softPushbackEnabled, dynamicRectangleColliders, decodedInputHolder, prevDecodedInputHolder, residueCollided, trapLocalIdToColliderAttrs, triggerTrackingIdToTrapLocalId, completelyStaticTrapColliders, unconfirmedBattleResult, ref confirmedBattleResult, pushbackFrameLogBuffer, frameLogEnabled, playerRdfId, shouldDetectRealtimeRenderHistoryCorrection, out hasIncorrectlyPredictedRenderFrame, historyRdfHolder, _loggerBridge);
            if (hasIncorrectlyPredictedRenderFrame) {   
                Debug.Log(String.Format("@playerRdfId={0}, hasIncorrectlyPredictedRenderFrame=true for i:{1} -> i+1:{2}", playerRdfId, i, i+1));
            }

            if (frameLogEnabled) {
                rdfIdToActuallyUsedInput[i] = delayedInputFrame.Clone();
            }

            var (ok3, nextRdf) = renderBuffer.GetByFrameId(i + 1);
            if (false == ok3 || null == nextRdf) {
                if (isChasing) { 
                    throw new ArgumentNullException(String.Format("Couldn't find nextRdf for i+1={0} to rollback, playerRdfId={1}: renderBuffer StFrameId={2}, EdFrameId={3}, Cnt={4}", i + 1, playerRdfId, renderBuffer.StFrameId, renderBuffer.EdFrameId, renderBuffer.Cnt));
                } else {
                    throw new ArgumentNullException(String.Format("Couldn't find nextRdf for i+1={0} to generate, playerRdfId={1} while rendering: renderBuffer StFrameId={2}, EdFrameId={3}, Cnt={4}", i + 1, playerRdfId, renderBuffer.StFrameId, renderBuffer.EdFrameId, renderBuffer.Cnt));
                }
            }
            if (nextRdf.Id != i + 1) {
                throw new ArgumentException(String.Format("Corrupted historic rdf for i+1={0} to rollback/generate, nextRdf={1}!", i, nextRdf));
            }
            if (true == isChasing) {
                // [WARNING] Move the cursor "chaserRenderFrameId" when "true == isChasing", keep in mind that "chaserRenderFrameId" is not monotonic!
                chaserRenderFrameId = nextRdf.Id;
            } else if (nextRdf.Id == chaserRenderFrameId + 1) {
                chaserRenderFrameId = nextRdf.Id; // To avoid redundant calculation 
            }
            prevLatestRdf = currRdf;
            latestRdf = nextRdf;
        }

        return (prevLatestRdf, latestRdf);
    }

    private int _markConfirmationIfApplicable() {
        int newAllConfirmedCnt = 0;
        int candidateInputFrameId = (lastAllConfirmedInputFrameId + 1);
        if (candidateInputFrameId < inputBuffer.StFrameId) {
            candidateInputFrameId = inputBuffer.StFrameId;
        }
        while (inputBuffer.StFrameId <= candidateInputFrameId && candidateInputFrameId < inputBuffer.EdFrameId) {
            var (res1, inputFrameDownsync) = inputBuffer.GetByFrameId(candidateInputFrameId);
            if (false == res1 || null == inputFrameDownsync) break;
            if (false == isAllConfirmed(inputFrameDownsync.ConfirmedList, roomCapacity)) break;
            ++candidateInputFrameId;
            ++newAllConfirmedCnt;
        }
        if (0 < newAllConfirmedCnt) {
            lastAllConfirmedInputFrameId = candidateInputFrameId - 1;
        }
        return newAllConfirmedCnt;
    }

    protected void _handleIncorrectlyRenderedPrediction(int firstPredictedYetIncorrectInputFrameId, bool fromUDP) {
        if (TERMINATING_INPUT_FRAME_ID == firstPredictedYetIncorrectInputFrameId) return;
        int renderFrameId1 = ConvertToFirstUsedRenderFrameId(firstPredictedYetIncorrectInputFrameId);
        if (renderFrameId1 >= chaserRenderFrameId) return;

        /*
		   A typical case is as follows.
		   --------------------------------------------------------
		   <renderFrameId1>                           :              36


		   <this.chaserRenderFrameId>                 :              62

		   [this.renderFrameId]                       :              64
		   --------------------------------------------------------
		 */
    
        // The actual rollback-and-chase would later be executed in "Update()". 
        chaserRenderFrameId = renderFrameId1;

        int rollbackFrames = (playerRdfId - chaserRenderFrameId);
        if (0 >= rollbackFrames) {
            // The incorrect prediction is not yet rendered, no visual impact for player.
            rollbackFrames = 0;
        } else {
            /* 
            [WARNING] The incorrect prediction was already rendered, there MIGHT BE a visual impact for player.

            However, due to the use of 
            - `UpdateInputFrameInPlaceUponDynamics`, and  
            - `processInertiaWalking` 
            , even if an "inputFrame" for "already rendered renderFrame" was incorrectly predicted, there's still chance that no visual impact is induced. See relevant sections in `README` for details.  

            Printing of this message might induce a performance impact.
            
            TODO: Instead of printing, add frameLog for (currRenderFrameId, rolledBackInputFrameDownsyncId, rolledBackToRenderFrameId)!
             */
            Debug.Log(String.Format("@playerRdfId={5}, mismatched input for rendered history detected, resetting chaserRenderFrameId: {0}->{1}; firstPredictedYetIncorrectInputFrameId: {2}, lastAllConfirmedInputFrameId={3}, fromUDP={4}", chaserRenderFrameId, renderFrameId1, firstPredictedYetIncorrectInputFrameId, lastAllConfirmedInputFrameId, fromUDP, playerRdfId));
        }
        NetworkDoctor.Instance.LogRollbackFrames(rollbackFrames);
    }

    public void applyRoomDownsyncFrameDynamics(RoomDownsyncFrame rdf, RoomDownsyncFrame prevRdf) {
        if (isBattleResultSet(confirmedBattleResult)) {
            StartCoroutine(delayToShowSettlementPanel());
            return;
        }

        // Put teamRibbons and hpBars to infinitely far first
        for (int i = cachedTeamRibbons.vals.StFrameId; i < cachedTeamRibbons.vals.EdFrameId; i++) {
            var (res, teamRibbon) = cachedTeamRibbons.vals.GetByFrameId(i);
            if (!res || null == teamRibbon) throw new ArgumentNullException(String.Format("There's no cachedTeamRibbon for i={0}, while StFrameId={1}, EdFrameId={2}", i, cachedTeamRibbons.vals.StFrameId, cachedTeamRibbons.vals.EdFrameId));

            newPosHolder.Set(effectivelyInfinitelyFar, effectivelyInfinitelyFar, inplaceHpBarZ);
            teamRibbon.gameObject.transform.position = newPosHolder;
        }

        for (int i = cachedHpBars.vals.StFrameId; i < cachedHpBars.vals.EdFrameId; i++) {
            var (res, hpBar) = cachedHpBars.vals.GetByFrameId(i);
            if (!res || null == hpBar) throw new ArgumentNullException(String.Format("There's no cachedHpBar for i={0}, while StFrameId={1}, EdFrameId={2}", i, cachedHpBars.vals.StFrameId, cachedHpBars.vals.EdFrameId));

            newPosHolder.Set(effectivelyInfinitelyFar, effectivelyInfinitelyFar, inplaceHpBarZ);
            hpBar.gameObject.transform.position = newPosHolder;
        }

        // Put "Tracing" vfxNodes to infinitely far first
        foreach (var entry in cachedVfxNodes) {
            var speciesId = entry.Key;
            var vfxConfig = vfxDict[speciesId];
            if (VfxMotionType.Tracing != vfxConfig.MotionType) continue;
            var speciesKvPq = entry.Value;
            for (int i = speciesKvPq.vals.StFrameId; i < speciesKvPq.vals.EdFrameId; i++) {
                var (res, vfxAnimHolder) = speciesKvPq.vals.GetByFrameId(i);
                if (!res || null == vfxAnimHolder) throw new ArgumentNullException(String.Format("There's no vfxAnimHolder for i={0}, while StFrameId={1}, EdFrameId={2}", i, speciesKvPq.vals.StFrameId, speciesKvPq.vals.EdFrameId));

                newPosHolder.Set(effectivelyInfinitelyFar, effectivelyInfinitelyFar, vfxAnimHolder.gameObject.transform.position.z);
                vfxAnimHolder.gameObject.transform.position = newPosHolder;
            }
        }

        float selfPlayerWx = 0f, selfPlayerWy = 0f;

        for (int k = 0; k < roomCapacity; k++) {
            var currCharacterDownsync = rdf.PlayersArr[k];
            var prevCharacterDownsync = (null == prevRdf ? null : prevRdf.PlayersArr[k]);
            //Debug.Log(String.Format("At rdf.Id={0}, currCharacterDownsync[k:{1}] at [vGridX: {2}, vGridY: {3}, velX: {4}, velY: {5}, chState: {6}, framesInChState: {7}, dirx: {8}]", rdf.Id, k, currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY, currCharacterDownsync.VelX, currCharacterDownsync.VelY, currCharacterDownsync.CharacterState, currCharacterDownsync.FramesInChState, currCharacterDownsync.DirX));
            var (collisionSpaceX, collisionSpaceY) = VirtualGridToPolygonColliderCtr(currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY);

            var chConfig = characters[currCharacterDownsync.SpeciesId];
            float boxCx, boxCy, boxCw, boxCh;
            calcCharacterBoundingBoxInCollisionSpace(currCharacterDownsync, chConfig, currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY, out boxCx, out boxCy, out boxCw, out boxCh);
            var (wx, wy) = CollisionSpacePositionToWorldPosition(boxCx, boxCy, spaceOffsetX, spaceOffsetY);

            if (null != prevCharacterDownsync && currCharacterDownsync.SpeciesId != prevCharacterDownsync.SpeciesId) {
                Destroy(playerGameObjs[k]);
                spawnPlayerNode(k+1, chConfig.SpeciesId, wx, wy, currCharacterDownsync.BulletTeamId);
            }

            var playerGameObj = playerGameObjs[k];
            newPosHolder.Set(wx, wy, playerGameObj.transform.position.z);

            playerGameObj.transform.position = newPosHolder; // [WARNING] Even if not selfPlayer, we have to set position of the other players regardless of new positions being visible within camera or not, otherwise outdated other players' node might be rendered within camera! 

            if (currCharacterDownsync.JoinIndex == selfPlayerInfo.JoinIndex) {
                selfBattleHeading.SetCharacter(currCharacterDownsync);
                selfPlayerWx = wx;
                selfPlayerWy = wy;
                //newPosHolder.Set(wx, wy, playerGameObj.transform.position.z);
                //selfPlayerLights.gameObject.transform.position = newPosHolder;
                //selfPlayerLights.setDirX(currCharacterDownsync.DirX);

                for (int i = 0; i < currCharacterDownsync.Inventory.Slots.Count; i++) {
                    var slotData = currCharacterDownsync.Inventory.Slots[i];
                    if (InventorySlotStockType.NoneIv == slotData.StockType) break;
                    var targetBtn = (0 == i ? iptmgr.btnC : iptmgr.btnD); // TODO: Don't hardcode them
                    var ivSlotGui = targetBtn.GetComponent<InventorySlot>();
                    ivSlotGui.updateData(slotData);
                }
            } else {
                float halfBoxCh = .5f * boxCh;
                float halfBoxCw = .5f * boxCw;
                newTlPosHolder.Set(wx - halfBoxCw, wy + halfBoxCh, characterZ);
                newTrPosHolder.Set(wx + halfBoxCw, wy + halfBoxCh, characterZ);
                newBlPosHolder.Set(wx - halfBoxCw, wy - halfBoxCh, characterZ);
                newBrPosHolder.Set(wx + halfBoxCw, wy - halfBoxCh, characterZ);

                if (!isGameObjPositionWithinCamera(newTlPosHolder) && !isGameObjPositionWithinCamera(newTrPosHolder) && !isGameObjPositionWithinCamera(newBlPosHolder) && !isGameObjPositionWithinCamera(newBrPosHolder)) {
                    // No need to update the actual anim if the other players are out of sight
                    continue;
                }

                // Add teamRibbon and inplaceHpBar
                showTeamRibbonAndInplaceHpBar(rdf.Id, currCharacterDownsync, wx, wy, halfBoxCw, halfBoxCh, "pl-" + currCharacterDownsync.JoinIndex);
            }

            var chAnimCtrl = playerGameObj.GetComponent<CharacterAnimController>();
            if (SPECIES_GUNGIRL == chConfig.SpeciesId) {
                chAnimCtrl.updateTwoPartsCharacterAnim(currCharacterDownsync, currCharacterDownsync.CharacterState, prevCharacterDownsync, false, chConfig, effectivelyInfinitelyFar);
            } else {
                chAnimCtrl.updateCharacterAnim(currCharacterDownsync, currCharacterDownsync.CharacterState, prevCharacterDownsync, false, chConfig);
            }

            // Add character vfx
            float distanceAttenuationZ = Math.Abs(wx - selfPlayerWx) + Math.Abs(wy - selfPlayerWy);
            if (SPECIES_GUNGIRL == chConfig.SpeciesId) {
                playCharacterDamagedVfx(currCharacterDownsync, chConfig, prevCharacterDownsync, chAnimCtrl.upperPart.gameObject, chAnimCtrl);
            } else {
                playCharacterDamagedVfx(currCharacterDownsync, chConfig, prevCharacterDownsync, playerGameObj, chAnimCtrl);
            }
            playCharacterSfx(currCharacterDownsync, prevCharacterDownsync, chConfig, wx, wy, rdf.Id, distanceAttenuationZ);
            playCharacterVfx(currCharacterDownsync, prevCharacterDownsync, chConfig, wx, wy, rdf.Id);
        }

        // Put all npcNodes to infinitely far first
        foreach (var entry in cachedNpcs) {
            var speciesId = entry.Key;
            var speciesKvPq = entry.Value;
            for (int i = speciesKvPq.vals.StFrameId; i < speciesKvPq.vals.EdFrameId; i++) {
                var (res, npcAnimHolder) = speciesKvPq.vals.GetByFrameId(i);
                if (!res || null == npcAnimHolder) throw new ArgumentNullException(String.Format("There's no npcAnimHolder for i={0}, while StFrameId={1}, EdFrameId={2}", i, speciesKvPq.vals.StFrameId, speciesKvPq.vals.EdFrameId));

                newPosHolder.Set(effectivelyInfinitelyFar, effectivelyInfinitelyFar, npcAnimHolder.gameObject.transform.position.z);
                npcAnimHolder.gameObject.transform.position = newPosHolder;
            }
        }

        for (int k = 0; k < rdf.NpcsArr.Count; k++) {
            var currNpcDownsync = rdf.NpcsArr[k];

            if (TERMINATING_PLAYER_ID == currNpcDownsync.Id) break;
            var prevNpcDownsync = (null == prevRdf ? null : prevRdf.NpcsArr[k]);
            // Debug.Log(String.Format("At rdf.Id={0}, currNpcDownsync[k:{1}] at [vx: {2}, vy: {3}, chState: {4}, framesInChState: {5}]", rdf.Id, k, currNpcDownsync.VirtualGridX, currNpcDownsync.VirtualGridY, currNpcDownsync.CharacterState, currNpcDownsync.FramesInChState));

            var chConfig = characters[currNpcDownsync.SpeciesId];
            float boxCx, boxCy, boxCw, boxCh;
            calcCharacterBoundingBoxInCollisionSpace(currNpcDownsync, chConfig, currNpcDownsync.VirtualGridX, currNpcDownsync.VirtualGridY, out boxCx, out boxCy, out boxCw, out boxCh);
            var (wx, wy) = CollisionSpacePositionToWorldPosition(boxCx, boxCy, spaceOffsetX, spaceOffsetY);

            float halfBoxCh = .5f * boxCh;
            float halfBoxCw = .5f * boxCw;
            newTlPosHolder.Set(wx - halfBoxCw, wy + halfBoxCh, characterZ);
            newTrPosHolder.Set(wx + halfBoxCw, wy + halfBoxCh, characterZ);
            newBlPosHolder.Set(wx - halfBoxCw, wy - halfBoxCh, characterZ);
            newBrPosHolder.Set(wx + halfBoxCw, wy - halfBoxCh, characterZ);
            
            if (!isGameObjPositionWithinCamera(newTlPosHolder) && !isGameObjPositionWithinCamera(newTrPosHolder) && !isGameObjPositionWithinCamera(newBlPosHolder) && !isGameObjPositionWithinCamera(newBrPosHolder)) continue;
            // if the current position is within camera FOV
            var speciesKvPq = cachedNpcs[currNpcDownsync.SpeciesId];
            string lookupKey = "npc-" + currNpcDownsync.Id;
            var npcAnimHolder = speciesKvPq.PopAny(lookupKey);
            if (null == npcAnimHolder) {
                npcAnimHolder = speciesKvPq.Pop();
                //Debug.Log(String.Format("@rdf.Id={0} using a new npcAnimHolder for rendering for npcId={1}, joinIndex={2} at wpos=({3}, {4})", rdf.Id, currNpcDownsync.Id, currNpcDownsync.JoinIndex, currNpcDownsync.VirtualGridX, currNpcDownsync.VirtualGridY));
            } else {
                //Debug.Log(String.Format("@rdf.Id={0} using a cached vfxAnimHolder for rendering for npcId={1}, joinIndex={2} at wpos=({3}, {4})", rdf.Id, currNpcDownsync.Id, currNpcDownsync.JoinIndex, currNpcDownsync.VirtualGridX, currNpcDownsync.VirtualGridY));
            }

            if (null == npcAnimHolder) {
                throw new ArgumentNullException(String.Format("No available npcAnimHolder node for lookupKey={0}", lookupKey));
            }

            var npcGameObj = npcAnimHolder.gameObject;
            newPosHolder.Set(wx, wy, characterZ);
            npcGameObj.transform.position = newPosHolder;

            npcAnimHolder.updateCharacterAnim(currNpcDownsync, currNpcDownsync.CharacterState, prevNpcDownsync, false, chConfig);
            npcAnimHolder.score = rdf.Id;
            speciesKvPq.Put(lookupKey, npcAnimHolder);

            // Add teamRibbon and inplaceHpBar
            showTeamRibbonAndInplaceHpBar(rdf.Id, currNpcDownsync, wx, wy, halfBoxCw, halfBoxCh, lookupKey);

            // Add character vfx
            if (currNpcDownsync.NewBirth) {
                var spr = npcGameObj.GetComponent<SpriteRenderer>();
                var material = spr.material;
                DOTween.Sequence().Append(
                    DOTween.To(() => material.GetFloat(MATERIAL_REF_THICKNESS), x => material.SetFloat(MATERIAL_REF_THICKNESS, x), DAMAGED_THICKNESS, DAMAGED_BLINK_SECONDS_HALF))
                    .Append(DOTween.To(() => material.GetFloat(MATERIAL_REF_THICKNESS), x => material.SetFloat(MATERIAL_REF_THICKNESS, x), 0f, DAMAGED_BLINK_SECONDS_HALF));
            }
            playCharacterDamagedVfx(currNpcDownsync, chConfig, prevNpcDownsync, npcGameObj, npcAnimHolder);
            float distanceAttenuationZ = Math.Abs(wx - selfPlayerWx) + Math.Abs(wy - selfPlayerWy);
            playCharacterSfx(currNpcDownsync, prevNpcDownsync, chConfig, wx, wy, rdf.Id, distanceAttenuationZ);
            playCharacterVfx(currNpcDownsync, prevNpcDownsync, chConfig, wx, wy, rdf.Id);
        }

        int kDynamicTrap = 0;
        for (int k = 0; k < rdf.TrapsArr.Count; k++) {
            var currTrap = rdf.TrapsArr[k];
            if (TERMINATING_TRAP_ID == currTrap.TrapLocalId) break;
            if (currTrap.IsCompletelyStatic) continue;
            var (collisionSpaceX, collisionSpaceY) = VirtualGridToPolygonColliderCtr(currTrap.VirtualGridX, currTrap.VirtualGridY);
            var (wx, wy) = CollisionSpacePositionToWorldPosition(collisionSpaceX, collisionSpaceY, spaceOffsetX, spaceOffsetY);
            var dynamicTrapObj = dynamicTrapGameObjs[kDynamicTrap];
            newPosHolder.Set(wx, wy, dynamicTrapObj.transform.position.z);
            dynamicTrapObj.transform.position = newPosHolder;
            var chAnimCtrl = dynamicTrapObj.GetComponent<TrapAnimationController>();
            chAnimCtrl.updateAnim("Tidle", 0, 0, true); // TODO: remove the hardcoded value
            kDynamicTrap++;
        }

        // Put all fireball nodes to infinitely far first
        for (int i = cachedFireballs.vals.StFrameId; i < cachedFireballs.vals.EdFrameId; i++) {
            var (res, fireballHolder) = cachedFireballs.vals.GetByFrameId(i);
            if (!res || null == fireballHolder) throw new ArgumentNullException(String.Format("There's no fireballHolder for i={0}, while StFrameId={1}, EdFrameId={2}", i, cachedFireballs.vals.StFrameId, cachedFireballs.vals.EdFrameId));

            newPosHolder.Set(effectivelyInfinitelyFar, effectivelyInfinitelyFar, fireballHolder.gameObject.transform.position.z);
            fireballHolder.gameObject.transform.position = newPosHolder;
        }

        for (int k = 0; k < rdf.Bullets.Count; k++) {
            var bullet = rdf.Bullets[k];
            if (TERMINATING_BULLET_LOCAL_ID == bullet.BattleAttr.BulletLocalId) break;
            
            var (cx, cy) = VirtualGridToPolygonColliderCtr(bullet.VirtualGridX, bullet.VirtualGridY);
            var (boxCw, boxCh) = VirtualGridToPolygonColliderCtr(bullet.Config.HitboxSizeX, bullet.Config.HitboxSizeY);
            var (wx, wy) = CollisionSpacePositionToWorldPosition(cx, cy, spaceOffsetX, spaceOffsetY);

            float halfBoxCh = .5f * boxCh;
            float halfBoxCw = .5f * boxCw;
            newTlPosHolder.Set(wx - halfBoxCw, wy + halfBoxCh, 0);
            newTrPosHolder.Set(wx + halfBoxCw, wy + halfBoxCh, 0);
            newBlPosHolder.Set(wx - halfBoxCw, wy - halfBoxCh, 0);
            newBrPosHolder.Set(wx + halfBoxCw, wy - halfBoxCh, 0);

            if (!isGameObjPositionWithinCamera(newTlPosHolder) && !isGameObjPositionWithinCamera(newTrPosHolder) && !isGameObjPositionWithinCamera(newBlPosHolder) && !isGameObjPositionWithinCamera(newBrPosHolder)) continue;

            bool isExploding = IsBulletExploding(bullet);
            var skillConfig = skills[bullet.BattleAttr.SkillId];
            var prevHitBulletConfig = (0 < bullet.BattleAttr.ActiveSkillHit ? skillConfig.Hits[bullet.BattleAttr.ActiveSkillHit - 1] : null); // TODO: Make this compatible with simultaneous bullets after a "FromPrevHitXxx" bullet!
            bool isInPrevHitTriggeredMultiHitSubsequence = (null != prevHitBulletConfig && (MultiHitType.FromPrevHitActual == prevHitBulletConfig.MhType || MultiHitType.FromPrevHitAnyway == prevHitBulletConfig.MhType));

            string lookupKey = bullet.BattleAttr.BulletLocalId.ToString(), animName = null;
            bool spontaneousLooping = false;
            
            int explosionSpeciesId = bullet.Config.ExplosionSpeciesId;
            if (EXPLOSION_SPECIES_FOLLOW == explosionSpeciesId) {
                explosionSpeciesId = bullet.Config.SpeciesId;
            }
            switch (bullet.Config.BType) {
                case BulletType.Melee:
                    if (isExploding) {
                        animName = String.Format("Melee_Explosion{0}", explosionSpeciesId);
                    }
                    break;
                case BulletType.Fireball:
                    if (IsBulletActive(bullet, rdf.Id) || isInPrevHitTriggeredMultiHitSubsequence || isExploding) {
                        animName = isExploding ? String.Format("Explosion{0}", explosionSpeciesId) : String.Format("Fireball{0}", bullet.Config.SpeciesId);
                        spontaneousLooping = !isExploding;
                    }
                    break;
                default:
                    break;
            }
            if (null != animName) {
                var explosionAnimHolder = cachedFireballs.PopAny(lookupKey);
                if (null == explosionAnimHolder) {
                    explosionAnimHolder = cachedFireballs.Pop();
                    //Debug.Log(String.Format("@rdf.Id={0}, origRdfId={1} using a new fireball node for rendering for bulletLocalId={2}, btype={3} at wpos=({4}, {5})", rdf.Id, bullet.BattleAttr.OriginatedRenderFrameId, bullet.BattleAttr.BulletLocalId, bullet.Config.BType, wx, wy));
                } else {
                    //Debug.Log(String.Format("@rdf.Id={0}, origRdfId={1} using a cached node for rendering for bulletLocalId={2}, btype={3} at wpos=({4}, {5})", rdf.Id, bullet.BattleAttr.OriginatedRenderFrameId, bullet.BattleAttr.BulletLocalId, bullet.Config.BType, wx, wy));
                }

                if (null != explosionAnimHolder) {
                    if (explosionAnimHolder.lookUpTable.ContainsKey(animName)) {
                        explosionAnimHolder.updateAnim(animName, bullet.FramesInBlState, bullet.DirX, spontaneousLooping, bullet.Config, rdf);
                        newPosHolder.Set(wx, wy, explosionAnimHolder.gameObject.transform.position.z);
                        explosionAnimHolder.gameObject.transform.position = newPosHolder;
                    }
                    explosionAnimHolder.score = rdf.Id;
                    cachedFireballs.Put(lookupKey, explosionAnimHolder);
                } else {
                    // null == explosionAnimHolder
                    if (EXPLOSION_SPECIES_NONE != explosionSpeciesId) {
                        // Explosion of fireballs is now allowed to use pure particle vfx
                        throw new ArgumentNullException(String.Format("No available fireball node for lookupKey={0}, animName={1}", lookupKey, animName));
                    }
                }
            }
            
            float distanceAttenuationZ = Math.Abs(wx - selfPlayerWx) + Math.Abs(wy - selfPlayerWy);
            playBulletSfx(bullet, isExploding, wx, wy, rdf.Id, distanceAttenuationZ);
            playBulletVfx(bullet, isExploding, wx, wy, rdf.Id);
        }

        for (int k = 0; k < rdf.TriggersArr.Count; k++) {
            var trigger = rdf.TriggersArr[k];
            if (TERMINATING_TRIGGER_ID == trigger.TriggerLocalId) break;
            var triggerGameObj = triggerGameObjs[trigger.TriggerLocalId];
            var animCtrl = triggerGameObj.GetComponent<TrapAnimationController>();
            if (TRIGGER_MASK_BY_CYCLIC_TIMER == trigger.Config.TriggerMask) {
                animCtrl.updateAnim(trigger.State.ToString(), trigger.FramesInState, trigger.ConfigFromTiled.InitVelX, false);
            } else {    
                // TODO: Make use fo TriggerState in "shared.Battle_dynamics"!
                if (Battle.isTriggerClickable(trigger)) {
                    animCtrl.updateAnim("Tready", 0, 0, true);
                } else {
                    animCtrl.updateAnim("TcoolingDown", 0, 0, true);
                }
            }
        }
    }

    protected void preallocateSfxNodes() {
        // TODO: Shall I use the same preallocation strategy for VFX? Would run for a while and see the difference...
        Debug.Log("preallocateSfxNodes begins");
        if (null != cachedSfxNodes) {
            while (0 < cachedSfxNodes.Cnt()) {
                var g = cachedSfxNodes.Pop();
                if (null != g) {
                    Destroy(g.gameObject);
                }
            }
        }
        int sfxNodeCacheCapacity = 64;
        cachedSfxNodes = new KvPriorityQueue<string, SFXSource>(sfxNodeCacheCapacity, sfxNodeScore);
        string[] allSfxClipsNames = new string[] {
            "Explosion1",
            "Explosion2",
            "Explosion3",
            "Explosion4",
            "Explosion8",
            "Melee_Explosion1",
            "Melee_Explosion2",
            "Melee_Explosion3",
            "Fireball8",
            "FlameBurning1",
            "FlameEmit1",
            "SlashEmitSpd1",
            "SlashEmitSpd2",
            "SlashEmitSpd3",
            "FootStep1",
            "DoorOpen",
            "DoorClose", 
        };
        var audioClipDict = new Dictionary<string, AudioClip>();
        foreach (string name in allSfxClipsNames) {
            string prefabPathUnderResources = "SFX/" + name;
            var theClip = Resources.Load(prefabPathUnderResources) as AudioClip;
            audioClipDict[name] = theClip;
        }

        for (int i = 0; i < sfxNodeCacheCapacity; i++) {
            GameObject newSfxNode = Instantiate(sfxSourcePrefab, new Vector3(effectivelyInfinitelyFar, effectivelyInfinitelyFar, fireballZ), Quaternion.identity);
            SFXSource newSfxSource = newSfxNode.GetComponent<SFXSource>();
            newSfxSource.score = -1;
            newSfxSource.maxDistanceInWorld = effectivelyInfinitelyFar * 0.25f;
            newSfxSource.audioClipDict = audioClipDict;
            var initLookupKey = i.ToString();
            cachedSfxNodes.Put(initLookupKey, newSfxSource);
        }
        
        Debug.Log("preallocateSfxNodes ends");
    }

    protected void preallocateVfxNodes() {
        Debug.Log("preallocateVfxNodes begins");
        if (null != cachedVfxNodes) {
            foreach (var (_,v) in cachedVfxNodes) {
                while (0 < v.Cnt()) {
                    var g = v.Pop();
                    if (null != g) {
                        Destroy(g.gameObject);
                    }
                }
            }
        }
        cachedVfxNodes = new Dictionary<int, KvPriorityQueue<string, VfxNodeController>>();
        vfxSpeciesPrefabDict = new Dictionary<int, GameObject>();
        int cacheCapacityPerSpeciesId = 4;
        string[] allVfxPrefabNames = new string[] {
            "1_Dashing_Active",
            "2_Fire_Exploding_Big",
            "3_Ice_Exploding_Big",
            "4_Fire_Slash_Active",
            "5_Slash_Active",
            "6_Spike_Slash_Exploding",
            "7_Fire_PointLight_Active",
            "8_Pistol_Bullet_Exploding",
            "9_Slash_Exploding",
            "10_Ice_Lingering",
            "11_Xform"
        };

        foreach (string name in allVfxPrefabNames) {
            string speciesIdStr = name.Split("_")[0];
            int speciesId = speciesIdStr.ToInt();
            var cachedVfxNodesOfThisSpecies = new KvPriorityQueue<string, VfxNodeController>(cacheCapacityPerSpeciesId, vfxNodeScore);
            string prefabPathUnderResources = "VfxPrefabs/" + name;
            var thePrefab = Resources.Load(prefabPathUnderResources) as GameObject;
            vfxSpeciesPrefabDict[speciesId] = thePrefab;
            for (int i = 0; i < cacheCapacityPerSpeciesId; i++) {
                GameObject newVfxNode = Instantiate(thePrefab, new Vector3(effectivelyInfinitelyFar, effectivelyInfinitelyFar, fireballZ), Quaternion.identity);
                VfxNodeController newVfxNodeController = newVfxNode.GetComponent<VfxNodeController>();
                newVfxNodeController.score = -1;
                newVfxNodeController.speciesId = speciesId;
                var initLookupKey = i.ToString();
                cachedVfxNodesOfThisSpecies.Put(initLookupKey, newVfxNodeController);
            }
            cachedVfxNodes[speciesId] = cachedVfxNodesOfThisSpecies;
        }
        Debug.Log("preallocateVfxNodes ends");
    }

    protected void preallocateNpcNodes() {
        Debug.Log("preallocateNpcNodes begins");

        if (0 >= preallocNpcCapacity) {
            throw new ArgumentException(String.Format("preallocNpcCapacity={0} is non-positive, please initialize it first!", preallocNpcCapacity));
        }

        if (null != cachedNpcs) {
            foreach (var (_, v) in cachedNpcs) {
                while (0 < v.Cnt()) {
                    var g = v.Pop();
                    if (null != g) {
                        Destroy(g.gameObject);
                    }
                }
            }
        }

        var mapProps = underlyingMap.GetComponent<SuperCustomProperties>();
        CustomProperty npcPreallocCapDict;
        mapProps.TryGetCustomProperty("npcPreallocCapDict", out npcPreallocCapDict);
        if (null == npcPreallocCapDict || npcPreallocCapDict.IsEmpty) {
            throw new ArgumentNullException("No `npcPreallocCapDict` found on map-scope properties, it's required! Example\n\tvalue `1:16;3:15;4096:1` means that we preallocate 16 slots for species 1, 15 slots for species 3 and 1 slot for species 4096");
        }
        Dictionary<int, int> npcPreallocCapDictVal = new Dictionary<int, int>();
        string npcPreallocCapDictStr = npcPreallocCapDict.GetValueAsString();
        foreach (var kvPairPart in npcPreallocCapDictStr.Trim().Split(';')) {
            var intraKvPairParts = kvPairPart.Split(':');
            int speciesId = intraKvPairParts[0].Trim().ToInt();
            int speciesCapacity = intraKvPairParts[1].Trim().ToInt();
            npcPreallocCapDictVal[speciesId] = speciesCapacity;
        }
        npcSpeciesPrefabDict = new Dictionary<int, GameObject>();
        cachedNpcs = new Dictionary<int, KvPriorityQueue<string, CharacterAnimController>>();
        foreach (var kvPair in npcPreallocCapDictVal) {
            int speciesId = kvPair.Key;
            int speciesCapacity = kvPair.Value;
            var cachedNpcNodesOfThisSpecies = new KvPriorityQueue<string, CharacterAnimController>(speciesCapacity, cachedNpcScore);
            var thePrefab = loadCharacterPrefab(characters[speciesId]);
            npcSpeciesPrefabDict[speciesId] = thePrefab;
            for (int i = 0; i < speciesCapacity; i++) {
                GameObject newNpcNode = Instantiate(thePrefab, new Vector3(effectivelyInfinitelyFar, effectivelyInfinitelyFar, characterZ), Quaternion.identity);
                CharacterAnimController newNpcNodeController = newNpcNode.GetComponent<CharacterAnimController>();
                newNpcNodeController.score = -1;

                var initLookupKey = i.ToString();
                cachedNpcNodesOfThisSpecies.Put(initLookupKey, newNpcNodeController);
            }
            cachedNpcs[speciesId] = cachedNpcNodesOfThisSpecies;
        }

        Debug.Log("preallocateNpcNodes ends");
    }

    protected void preallocateHolders() {
        if (0 >= roomCapacity) {
            throw new ArgumentException(String.Format("roomCapacity={0} is non-positive, please initialize it first!", roomCapacity));
        }

        Debug.Log(String.Format("preallocateHolders with roomCapacity={0}, preallocNpcCapacity={1}, preallocBulletCapacity={2}", roomCapacity, preallocNpcCapacity, preallocBulletCapacity));
        int residueCollidedCap = 256;
        residueCollided = new FrameRingBuffer<shared.Collider>(residueCollidedCap);

        renderBufferSize = 1536;
        renderBuffer = new FrameRingBuffer<RoomDownsyncFrame>(renderBufferSize);
        for (int i = 0; i < renderBufferSize; i++) {
            renderBuffer.Put(NewPreallocatedRoomDownsyncFrame(roomCapacity, preallocNpcCapacity, preallocBulletCapacity, preallocTrapCapacity, preallocTriggerCapacity));
        }
        renderBuffer.Clear(); // Then use it by "DryPut"

        int softPushbacksCap = 16;
        if (frameLogEnabled) {
            pushbackFrameLogBuffer = new FrameRingBuffer<RdfPushbackFrameLog>(renderBufferSize);
            for (int i = 0; i < renderBufferSize; i++) {
                pushbackFrameLogBuffer.Put(new RdfPushbackFrameLog(TERMINATING_RENDER_FRAME_ID, roomCapacity + preallocNpcCapacity, softPushbacksCap));
            }
            pushbackFrameLogBuffer.Clear(); // Then use it by "DryPut"
        }

        int inputBufferSize = (renderBufferSize >> 1) + 1;
        inputBuffer = new FrameRingBuffer<InputFrameDownsync>(inputBufferSize);
        for (int i = 0; i < inputBufferSize; i++) {
            inputBuffer.Put(NewPreallocatedInputFrameDownsync(roomCapacity));
        }
        inputBuffer.Clear(); // Then use it by "DryPut"

        lastIndividuallyConfirmedInputFrameId = new int[roomCapacity];
        Array.Fill<int>(lastIndividuallyConfirmedInputFrameId, -1);

        lastIndividuallyConfirmedInputList = new ulong[roomCapacity];
        Array.Fill<ulong>(lastIndividuallyConfirmedInputList, 0);

        prefabbedInputListHolder = new ulong[roomCapacity];
        Array.Fill<ulong>(prefabbedInputListHolder, 0);

        effPushbacks = new Vector[roomCapacity + preallocNpcCapacity + preallocTrapCapacity];
        for (int i = 0; i < effPushbacks.Length; i++) {
            effPushbacks[i] = new Vector(0, 0);
        }
        hardPushbackNormsArr = new Vector[roomCapacity + preallocNpcCapacity + preallocTrapCapacity][];
        for (int i = 0; i < hardPushbackNormsArr.Length; i++) {
            int cap = 5;
            hardPushbackNormsArr[i] = new Vector[cap];
            for (int j = 0; j < cap; j++) {
                hardPushbackNormsArr[i][j] = new Vector(0, 0);
            }
        }

        softPushbacks = new Vector[softPushbacksCap];
        for (int i = 0; i < softPushbacks.Length; i++) {
            softPushbacks[i] = new Vector(0, 0);
        }
        softPushbackEnabled = true;

        int dynamicRectangleCollidersCap = 192;
        dynamicRectangleColliders = new shared.Collider[dynamicRectangleCollidersCap];
        staticColliders = new shared.Collider[128];

        decodedInputHolder = new InputFrameDecoded();
        prevDecodedInputHolder = new InputFrameDecoded();

        // fireball
        int fireballHoldersCap = 48;
        if (null != cachedFireballs) {
            for (int i = cachedFireballs.vals.StFrameId; i < cachedFireballs.vals.EdFrameId; i++) {
                var (res, fireball) = cachedFireballs.vals.GetByFrameId(i);
                if (!res || null == fireball) throw new ArgumentNullException(String.Format("There's no cachedFireball for i={0}, while StFrameId={1}, EdFrameId={2}", i, cachedFireballs.vals.StFrameId, cachedFireballs.vals.EdFrameId));
                Destroy(fireball.gameObject);
            }
        }
        cachedFireballs = new KvPriorityQueue<string, FireballAnimController>(fireballHoldersCap, cachedFireballScore);

        for (int i = 0; i < fireballHoldersCap; i++) {
            // Fireballs & explosions should be drawn above any character
            GameObject newFireballNode = Instantiate(fireballPrefab, Vector3.zero, Quaternion.identity);
            FireballAnimController holder = newFireballNode.GetComponent<FireballAnimController>();
            holder.score = -1;
            string initLookupKey = (-(i + 1)).ToString(); // there's definitely no such "bulletLocalId"
            cachedFireballs.Put(initLookupKey, holder);
        }

        // team ribbon
        int teamRibbonHoldersCap = roomCapacity+preallocNpcCapacity;
        if (null != cachedTeamRibbons) {
            for (int i = cachedTeamRibbons.vals.StFrameId; i < cachedTeamRibbons.vals.EdFrameId; i++) {
                var (res, teamRibbons) = cachedTeamRibbons.vals.GetByFrameId(i);
                if (!res || null == teamRibbons) throw new ArgumentNullException(String.Format("There's no cachedTeamRibbon for i={0}, while StFrameId={1}, EdFrameId={2}", i, cachedTeamRibbons.vals.StFrameId, cachedTeamRibbons.vals.EdFrameId));
                Destroy(teamRibbons.gameObject);
            }
        }
        cachedTeamRibbons = new KvPriorityQueue<string, TeamRibbon>(teamRibbonHoldersCap, cachedTeamRibbonScore);

        for (int i = 0; i < teamRibbonHoldersCap; i++) {
            GameObject newTeamRibbonNode = Instantiate(teamRibbonPrefab, Vector3.zero, Quaternion.identity);
            TeamRibbon holder = newTeamRibbonNode.GetComponent<TeamRibbon>();
            holder.score = -1;
            string initLookupKey = (-(i + 1)).ToString();
            cachedTeamRibbons.Put(initLookupKey, holder);
        }

        // hp bar
        if (null != cachedHpBars) {
            for (int i = cachedHpBars.vals.StFrameId; i < cachedHpBars.vals.EdFrameId; i++) {
                var (res, hpBar) = cachedHpBars.vals.GetByFrameId(i);
                if (!res || null == hpBar) throw new ArgumentNullException(String.Format("There's no cachedHpBar for i={0}, while StFrameId={1}, EdFrameId={2}", i, cachedHpBars.vals.StFrameId, cachedHpBars.vals.EdFrameId));
                Destroy(hpBar.gameObject);
            }
        }
        int hpBarHoldersCap = teamRibbonHoldersCap;
        cachedHpBars = new KvPriorityQueue<string, InplaceHpBar>(teamRibbonHoldersCap, cachedHpBarScore);

        for (int i = 0; i < hpBarHoldersCap; i++) {
            GameObject newHpBarNode = Instantiate(inplaceHpBarPrefab, Vector3.zero, Quaternion.identity);
            InplaceHpBar holder = newHpBarNode.GetComponent<InplaceHpBar>();
            holder.score = -1;
            string initLookupKey = (-(i + 1)).ToString();
            cachedHpBars.Put(initLookupKey, holder);
        }

        // debug line
        if (debugDrawingAllocation) {
            int lineHoldersCap = 64;
            if (null != cachedLineRenderers) {
                for (int i = cachedLineRenderers.vals.StFrameId; i < cachedLineRenderers.vals.EdFrameId; i++) {
                    var (res, line) = cachedLineRenderers.vals.GetByFrameId(i);
                    if (!res || null == line) throw new ArgumentNullException(String.Format("There's no line for i={0}, while StFrameId={1}, EdFrameId={2}", i, cachedLineRenderers.vals.StFrameId, cachedLineRenderers.vals.EdFrameId));
                    Destroy(line.gameObject);
                }
            }

            cachedLineRenderers = new KvPriorityQueue<string, DebugLine>(lineHoldersCap, cachedLineScore);
            for (int i = 0; i < lineHoldersCap; i++) {
                GameObject newLineObj = Instantiate(linePrefab, new Vector3(effectivelyInfinitelyFar, effectivelyInfinitelyFar, lineRendererZ), Quaternion.identity);
                DebugLine newLine = newLineObj.GetComponent<DebugLine>();
                newLine.score = -1;
                newLine.SetWidth(2.0f);
                var initLookupKey = i.ToString();
                cachedLineRenderers.Put(initLookupKey, newLine);
            }
        }

        confirmedBattleResult = new BattleResult {
            WinnerJoinIndex = MAGIC_JOIN_INDEX_DEFAULT
        };
    }

    protected virtual void resetCurrentMatch(string theme) {
        Debug.Log(String.Format("resetCurrentMatch with roomCapacity={0}", roomCapacity));
        battleState = ROOM_STATE_IMPOSSIBLE;
        playerRdfId = 0;
        renderFrameIdLagTolerance = 4;
        chaserRenderFrameId = -1;
        lastAllConfirmedInputFrameId = -1;
        lastUpsyncInputFrameId = -1;
        maxChasingRenderFramesPerUpdate = 5;
        rdfIdToActuallyUsedInput = new Dictionary<int, InputFrameDownsync>();
        trapLocalIdToColliderAttrs = new Dictionary<int, List<TrapColliderAttr>>();
        triggerTrackingIdToTrapLocalId = new Dictionary<int, int>();
        completelyStaticTrapColliders = new List<shared.Collider>();
        unconfirmedBattleResult = new Dictionary<int, BattleResult>();

        if (null != underlyingMap) {
            Destroy(underlyingMap);
        }
        playerGameObjs = new GameObject[roomCapacity];
        dynamicTrapGameObjs = new List<GameObject>();
        triggerGameObjs = new Dictionary<int, GameObject>();
        string path = String.Format("Tiled/{0}/map", theme);
        var underlyingMapPrefab = Resources.Load(path) as GameObject;
        underlyingMap = GameObject.Instantiate(underlyingMapPrefab);

        var superMap = underlyingMap.GetComponent<SuperMap>();
        int mapWidth = superMap.m_Width, tileWidth = superMap.m_TileWidth, mapHeight = superMap.m_Height, tileHeight = superMap.m_TileHeight;
        spaceOffsetX = ((mapWidth * tileWidth) >> 1);
        spaceOffsetY = ((mapHeight * tileHeight) >> 1);
        effectivelyInfinitelyFar = 4f * Math.Max(spaceOffsetX, spaceOffsetY);

        int cellWidth = 64;
        int cellHeight = 128; // To avoid dynamic trap as a standing point to slip when moving down along with the character
        collisionSys = new CollisionSpace(spaceOffsetX << 1, spaceOffsetY << 1, cellWidth, cellHeight);
        maxTouchingCellsCnt = (((spaceOffsetX << 1) + cellWidth) / cellWidth) * (((spaceOffsetY << 1) + cellHeight) / cellHeight) + 1;
        for (int i = 0; i < dynamicRectangleColliders.Length; i++) {
            dynamicRectangleColliders[i] = NewRectCollider(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, maxTouchingCellsCnt, null);
        }

        collisionHolder = new shared.Collision();

        // Reset the preallocated
        Array.Fill<int>(lastIndividuallyConfirmedInputFrameId, -1);
        Array.Fill<ulong>(lastIndividuallyConfirmedInputList, 0);
        renderBuffer.Clear();
        if (frameLogEnabled) {
            pushbackFrameLogBuffer.Clear();
        }
        inputBuffer.Clear();
        residueCollided.Clear();
        trapLocalIdToColliderAttrs.Clear();
        triggerTrackingIdToTrapLocalId.Clear();
        Array.Fill<ulong>(prefabbedInputListHolder, 0);

        resetBattleResult(ref confirmedBattleResult);
        readyGoPanel.resetCountdown();
        settlementPanel.gameObject.SetActive(false);
    }

    public void onInputFrameDownsyncBatch(Pbc.RepeatedField<InputFrameDownsync> batch) {
        // This method is guaranteed to run in UIThread only.
        if (null == batch) {
            return;
        }
        if (null == inputBuffer) {
            return;
        }
        if (ROOM_STATE_IN_SETTLEMENT == battleState) {
            return;
        }
        // Debug.Log(String.Format("onInputFrameDownsyncBatch called for batchInputFrameIdRange [{0}, {1}]", batch[0].InputFrameId, batch[batch.Count-1].InputFrameId));

        NetworkDoctor.Instance.LogInputFrameDownsync(batch[0].InputFrameId, batch[batch.Count - 1].InputFrameId);
        int firstPredictedYetIncorrectInputFrameId = TERMINATING_INPUT_FRAME_ID;
        foreach (var inputFrameDownsync in batch) {
            int inputFrameDownsyncId = inputFrameDownsync.InputFrameId;
            if (inputFrameDownsyncId <= lastAllConfirmedInputFrameId) {
                continue;
            }
            // [WARNING] Now that "inputFrameDownsyncId > self.lastAllConfirmedInputFrameId", we should make an update immediately because unlike its backend counterpart "Room.LastAllConfirmedInputFrameId", the frontend "mapIns.lastAllConfirmedInputFrameId" might inevitably get gaps among discrete values due to "either type#1 or type#2 forceConfirmation" -- and only "onInputFrameDownsyncBatch" can catch this! 
            lastAllConfirmedInputFrameId = inputFrameDownsyncId;
            var (res1, localInputFrame) = inputBuffer.GetByFrameId(inputFrameDownsyncId);
            if (null != localInputFrame
              &&
              TERMINATING_INPUT_FRAME_ID == firstPredictedYetIncorrectInputFrameId
              &&
              !shared.Battle.EqualInputLists(localInputFrame.InputList, inputFrameDownsync.InputList)
            ) {
                firstPredictedYetIncorrectInputFrameId = inputFrameDownsyncId;
            } else if (
                TERMINATING_INPUT_FRAME_ID == firstPredictedYetIncorrectInputFrameId
                && 
                unconfirmedBattleResult.ContainsKey(inputFrameDownsyncId)
                ) {
                // [WARNING] Unconfirmed battle results must be revisited!
                firstPredictedYetIncorrectInputFrameId = inputFrameDownsyncId;
                unconfirmedBattleResult.Remove(inputFrameDownsyncId);
            }
            // [WARNING] Take all "inputFrameDownsync" from backend as all-confirmed, it'll be later checked by "rollbackAndChase". 
            inputFrameDownsync.ConfirmedList = (1UL << roomCapacity) - 1;

            for (int j = 0; j < roomCapacity; j++) {
                if (inputFrameDownsync.InputFrameId > lastIndividuallyConfirmedInputFrameId[j]) {
                    lastIndividuallyConfirmedInputFrameId[j] = inputFrameDownsync.InputFrameId;
                    lastIndividuallyConfirmedInputList[j] = inputFrameDownsync.InputList[j];
                }
            }
            //console.log(`Confirmed inputFrameId=${inputFrameDownsync.inputFrameId}`);
            var (res2, oldStFrameId, oldEdFrameId) = inputBuffer.SetByFrameId(inputFrameDownsync, inputFrameDownsync.InputFrameId);
            if (RingBuffer<InputFrameDownsync>.RING_BUFF_FAILED_TO_SET == res2) {
                throw new ArgumentException(String.Format("Failed to dump input cache(maybe recentInputCache too small)! inputFrameDownsync.inputFrameId={0}, lastAllConfirmedInputFrameId={1}", inputFrameDownsyncId, lastAllConfirmedInputFrameId));
            }
        }
        _markConfirmationIfApplicable();
        _handleIncorrectlyRenderedPrediction(firstPredictedYetIncorrectInputFrameId, false);
    }

    public void onRoomDownsyncFrame(RoomDownsyncFrame pbRdf, Pbc::RepeatedField<InputFrameDownsync> accompaniedInputFrameDownsyncBatch) {
        // This function is also applicable to "re-joining".
        onInputFrameDownsyncBatch(accompaniedInputFrameDownsyncBatch); // Important to do this step before setting IN_BATTLE
        if (null == renderBuffer) {
            return;
        }
        if (ROOM_STATE_IN_SETTLEMENT == battleState) {
            return;
        }
        int rdfId = pbRdf.Id;
        bool shouldForceDumping1 = (Battle.DOWNSYNC_MSG_ACT_BATTLE_START == rdfId);
        bool shouldForceDumping2 = (rdfId >= playerRdfId + renderFrameIdLagTolerance);
        bool shouldForceResync = pbRdf.ShouldForceResync;
        ulong selfJoinIndexMask = ((ulong)1 << (selfPlayerInfo.JoinIndex - 1));
        bool notSelfUnconfirmed = (0 == (pbRdf.BackendUnconfirmedMask & selfJoinIndexMask));
        if (notSelfUnconfirmed) {
            shouldForceDumping2 = false;
            shouldForceResync = false;
            // othersForcedDownsyncRenderFrameDict.set(rdfId, pbRdf);
        }
        /*
		   If "BackendUnconfirmedMask" is non-all-1 and contains the current player, show a label/button to hint manual reconnection. Note that the continuity of "recentInputCache" is not a good indicator, because due to network delay upon a [type#1 forceConfirmation] a player might just lag in upsync networking and have all consecutive inputFrameIds locally. 
		 */

        var (dumpRenderCacheRet, oldStRenderFrameId, oldEdRenderFrameId) = (shouldForceDumping1 || shouldForceDumping2 || shouldForceResync) ? renderBuffer.SetByFrameId(pbRdf, rdfId) : (RingBuffer<RoomDownsyncFrame>.RING_BUFF_CONSECUTIVE_SET, TERMINATING_RENDER_FRAME_ID, TERMINATING_RENDER_FRAME_ID);

        if (RingBuffer<RoomDownsyncFrame>.RING_BUFF_FAILED_TO_SET == dumpRenderCacheRet) {
            throw new ArgumentException(String.Format("Failed to dump render cache#1 (maybe recentRenderCache too small)! rdfId={0}", rdfId));
        }

        if (!shouldForceResync && (Battle.DOWNSYNC_MSG_ACT_BATTLE_START < rdfId && RingBuffer<RoomDownsyncFrame>.RING_BUFF_CONSECUTIVE_SET == dumpRenderCacheRet)) {
            /*
			   Don't change 
			   - chaserRenderFrameId, it's updated only in "rollbackAndChase & onInputFrameDownsyncBatch" (except for when RING_BUFF_NON_CONSECUTIVE_SET)
			 */
            return;
        }

        if (shouldForceDumping1 || shouldForceDumping2 || shouldForceResync) {
            // In fact, not having "window.RING_BUFF_CONSECUTIVE_SET == dumpRenderCacheRet" should already imply that "renderFrameId <= rdfId", but here we double check and log the anomaly  
            if (Battle.DOWNSYNC_MSG_ACT_BATTLE_START == rdfId) {
                Debug.Log(String.Format("On battle started! received rdfId={0}", rdfId));
            } else {
                Debug.Log(String.Format("On battle resynced! received rdfId={0}", rdfId));
            }

            playerRdfId = rdfId;
            // In this case it must be true that "rdfId > chaserRenderFrameId".
            chaserRenderFrameId = rdfId;

            NetworkDoctor.Instance.LogRollbackFrames(0);

            battleState = ROOM_STATE_IN_BATTLE;
        }

        // [WARNING] Leave all graphical updates in "Update()" by "applyRoomDownsyncFrameDynamics"
        return;
    }

    // Update is called once per frame
    protected void doUpdate() {
        int noDelayInputFrameId = ConvertToNoDelayInputFrameId(playerRdfId);
        ulong prevSelfInput = 0, currSelfInput = 0;
        if (ShouldGenerateInputFrameUpsync(playerRdfId)) {
            (prevSelfInput, currSelfInput) = getOrPrefabInputFrameUpsync(noDelayInputFrameId, true, prefabbedInputListHolder);
        }
        int delayedInputFrameId = ConvertToDelayedInputFrameId(playerRdfId);
        var (delayedInputFrameExists, _) = inputBuffer.GetByFrameId(delayedInputFrameId);
        if (!delayedInputFrameExists) {
            // Possible edge case after resync, kindly note that it's OK to prefab a "future inputFrame" here, because "sendInputFrameUpsyncBatch" would be capped by "noDelayInputFrameId from self.renderFrameId". 
            getOrPrefabInputFrameUpsync(delayedInputFrameId, false, prefabbedInputListHolder);
        }

        bool battleResultIsSet = isBattleResultSet(confirmedBattleResult); 

        if (battleResultIsSet || shouldSendInputFrameUpsyncBatch(prevSelfInput, currSelfInput, noDelayInputFrameId)) {
            // [WARNING] If "true == battleResultIsSet", we MUST IMMEDIATELY flush the local inputs to our peers to favor the formation of all-confirmed inputFrameDownsync asap! 
            // TODO: Does the following statement run asynchronously in an implicit manner? Should I explicitly run it asynchronously?
            sendInputFrameUpsyncBatch(noDelayInputFrameId);
        }

        if (battleResultIsSet) {
            var (ok1, currRdf) = renderBuffer.GetByFrameId(playerRdfId - 1);
            if (ok1 && null != currRdf) {
                cameraTrack(currRdf, null);
            }
            return;
        }

        // Inside the following "rollbackAndChase" actually ROLLS FORWARD w.r.t. the corresponding delayedInputFrame, REGARDLESS OF whether or not "chaserRenderFrameId == renderFrameId" now. 
        var (prevRdf, rdf) = rollbackAndChase(playerRdfId, playerRdfId + 1, collisionSys, false);
        // Having "prevRdf.Id == renderFrameId" & "rdf.Id == renderFrameId+1" 

        applyRoomDownsyncFrameDynamics(rdf, prevRdf);
        cameraTrack(rdf, prevRdf);
        ++playerRdfId;
    }

    protected virtual int chaseRolledbackRdfs() {
        int prevChaserRenderFrameId = chaserRenderFrameId;
        int nextChaserRenderFrameId = (prevChaserRenderFrameId + maxChasingRenderFramesPerUpdate);

        if (nextChaserRenderFrameId > playerRdfId) {
            nextChaserRenderFrameId = playerRdfId;
        }

        if (prevChaserRenderFrameId < nextChaserRenderFrameId) {
            // Do not execute "rollbackAndChase" when "prevChaserRenderFrameId == nextChaserRenderFrameId", otherwise if "nextChaserRenderFrameId == self.renderFrameId" we'd be wasting computing power once. 
            rollbackAndChase(prevChaserRenderFrameId, nextChaserRenderFrameId, collisionSys, true);
        }

        return nextChaserRenderFrameId;
    }

    protected virtual void onBattleStopped() {
        if (ROOM_STATE_IN_BATTLE != battleState && ROOM_STATE_IN_SETTLEMENT != battleState) {
            return;
        }
        bgmSource.Stop();
        battleState = ROOM_STATE_STOPPED;

        iptmgr.reset();
    }

    private IEnumerator delayToShowSettlementPanel() {
        if (ROOM_STATE_IN_BATTLE != battleState) {
            yield return new WaitForSeconds(0);
        } else {
            battleState = ROOM_STATE_IN_SETTLEMENT;
            settlementPanel.gameObject.SetActive(true);
            settlementPanel.playFinishedAnim();
            yield return new WaitForSeconds(1);
            postSettlementCallback();
        }
    }

    protected abstract bool shouldSendInputFrameUpsyncBatch(ulong prevSelfInput, ulong currSelfInput, int currInputFrameId);

    protected abstract void sendInputFrameUpsyncBatch(int latestLocalInputFrameId);

    protected void enableBattleInput(bool yesOrNo) {
        iptmgr.enable(yesOrNo);
    }

    protected RoomDownsyncFrame mockStartRdf(int[] speciesIdList) {
        var grid = underlyingMap.GetComponentInChildren<Grid>();
        var playerStartingCposList = new List<(Vector, int, int)>();
        var npcsStartingCposList = new List<(Vector, int, int, int, int, bool)>();
        var trapList = new List<Trap>();
        var triggerList = new List<(Trigger, float, float)>();
        float defaultPatrolCueRadius = 10;
        int staticColliderIdx = 0;
        int trapLocalId = 0;
        int triggerLocalId = 0;
        foreach (Transform child in grid.transform) {
            switch (child.gameObject.name) {
                case "Barrier":
                    foreach (Transform barrierChild in child) {
                        var barrierTileObj = barrierChild.gameObject.GetComponent<SuperObject>();
                        var inMapCollider = barrierChild.gameObject.GetComponent<EdgeCollider2D>();

                        if (null == inMapCollider || 0 >= inMapCollider.pointCount) {
                            var (tiledRectCx, tiledRectCy) = (barrierTileObj.m_X + barrierTileObj.m_Width * 0.5f, barrierTileObj.m_Y + barrierTileObj.m_Height * 0.5f);
                            var (rectCx, rectCy) = TiledLayerPositionToCollisionSpacePosition(tiledRectCx, tiledRectCy, spaceOffsetX, spaceOffsetY);
                            /*
                             [WARNING] 

                            The "Unity World (0, 0)" is aligned with the top-left corner of the rendered "TiledMap (via SuperMap)".

                            It's noticeable that all the "Collider"s in "CollisionSpace" must be of positive coordinates to work due to the implementation details of "resolv". Thus I'm using a "Collision Space (0, 0)" aligned with the bottom-left of the rendered "TiledMap (via SuperMap)". 
                            */
                            var barrierCollider = NewRectCollider(rectCx, rectCy, barrierTileObj.m_Width, barrierTileObj.m_Height, 0, 0, 0, 0, 0, 0, maxTouchingCellsCnt, null);
                            //Debug.Log(String.Format("new barrierCollider=[X: {0}, Y: {1}, Width: {2}, Height: {3}]", barrierCollider.X, barrierCollider.Y, barrierCollider.W, barrierCollider.H));
                            staticColliders[staticColliderIdx++] = barrierCollider;
                        } else {
                            var points = inMapCollider.points;
                            List<float> points2 = new List<float>();
                            foreach (var point in points) {
                                points2.Add(point.x);
                                points2.Add(point.y);
                            }
                            var (anchorCx, anchorCy) = TiledLayerPositionToCollisionSpacePosition(barrierTileObj.m_X, barrierTileObj.m_Y, spaceOffsetX, spaceOffsetY);
                            var srcPolygon = new ConvexPolygon(anchorCx, anchorCy, points2.ToArray());
                            var barrierCollider = NewConvexPolygonCollider(srcPolygon, 0, 0, maxTouchingCellsCnt, null);

                            staticColliders[staticColliderIdx++] = barrierCollider;
                        }

                        // TODO: By now I have to enable the import of all colliders to see the "inMapCollider: EdgeCollider2D" component, then remove unused components here :(
                        Destroy(barrierChild.gameObject.GetComponent<EdgeCollider2D>());
                        Destroy(barrierChild.gameObject.GetComponent<BoxCollider2D>());
                        Destroy(barrierChild.gameObject.GetComponent<SuperColliderComponent>());
                    }
                    break;
                case "PlayerStartingPos":
                    int j = 0;
                    foreach (Transform playerPos in child) {
                        var posTileObj = playerPos.gameObject.GetComponent<SuperObject>();
                        var tileProps = playerPos.gameObject.gameObject.GetComponent<SuperCustomProperties>();
                        CustomProperty teamId, dirX;
                        tileProps.TryGetCustomProperty("teamId", out teamId);
                        tileProps.TryGetCustomProperty("dirX", out dirX);

                        var (cx, cy) = TiledLayerPositionToCollisionSpacePosition(posTileObj.m_X, posTileObj.m_Y, spaceOffsetX, spaceOffsetY);

                        playerStartingCposList.Add((
                            new Vector(cx, cy),
                            null == teamId || teamId.IsEmpty ? DEFAULT_BULLET_TEAM_ID : teamId.GetValueAsInt(),
                            null == dirX || dirX.IsEmpty ? +2 : dirX.GetValueAsInt()
                        ));
                        //Debug.Log(String.Format("new playerStartingCposList[i:{0}]=[X:{1}, Y:{2}]", j, cx, cy));
                        j++;
                    }
                    break;
                case "NpcStartingPos":
                    foreach (Transform npcPos in child) {
                        var tileObj = npcPos.gameObject.GetComponent<SuperObject>();
                        var tileProps = npcPos.gameObject.gameObject.GetComponent<SuperCustomProperties>();
                        var (cx, cy) = TiledLayerPositionToCollisionSpacePosition(tileObj.m_X, tileObj.m_Y, spaceOffsetX, spaceOffsetY);
                        CustomProperty dirX, dirY, speciesId, teamId, isStatic;
                        tileProps.TryGetCustomProperty("dirX", out dirX);
                        tileProps.TryGetCustomProperty("dirY", out dirY);
                        tileProps.TryGetCustomProperty("speciesId", out speciesId);
                        tileProps.TryGetCustomProperty("teamId", out teamId);
                        tileProps.TryGetCustomProperty("static", out isStatic);
                        npcsStartingCposList.Add((
                                                    new Vector(cx, cy),
                                                    null == dirX || dirX.IsEmpty ? 0 : dirX.GetValueAsInt(),
                                                    null == dirY || dirY.IsEmpty ? 0 : dirY.GetValueAsInt(),
                                                    null == speciesId || speciesId.IsEmpty ? 0 : speciesId.GetValueAsInt(),
                                                    null == teamId || teamId.IsEmpty ? DEFAULT_BULLET_TEAM_ID : teamId.GetValueAsInt(),
                                                    null == isStatic || isStatic.IsEmpty ? false : (1 == isStatic.GetValueAsInt())
                        ));
                    }
                    break;
                case "PatrolCue":
                    foreach (Transform patrolCueChild in child) {
                        var tileObj = patrolCueChild.gameObject.GetComponent<SuperObject>();
                        var tileProps = patrolCueChild.gameObject.GetComponent<SuperCustomProperties>();

                        var (patrolCueCx, patrolCueCy) = TiledLayerPositionToCollisionSpacePosition(tileObj.m_X, tileObj.m_Y, spaceOffsetX, spaceOffsetY);

                        CustomProperty id, flAct, frAct, flCaptureFrames, frCaptureFrames, fdAct, fuAct, fdCaptureFrames, fuCaptureFrames, collisionTypeMask;
                        tileProps.TryGetCustomProperty("id", out id);
                        tileProps.TryGetCustomProperty("flAct", out flAct);
                        tileProps.TryGetCustomProperty("frAct", out frAct);
                        tileProps.TryGetCustomProperty("flCaptureFrames", out flCaptureFrames);
                        tileProps.TryGetCustomProperty("frCaptureFrames", out frCaptureFrames);
                        tileProps.TryGetCustomProperty("fdAct", out fdAct);
                        tileProps.TryGetCustomProperty("fuAct", out fuAct);
                        tileProps.TryGetCustomProperty("fdCaptureFrames", out fdCaptureFrames);
                        tileProps.TryGetCustomProperty("fuCaptureFrames", out fuCaptureFrames);
                        tileProps.TryGetCustomProperty("collisionTypeMask", out collisionTypeMask);

                        ulong collisionTypeMaskVal = (null != collisionTypeMask && !collisionTypeMask.IsEmpty) ? (ulong)collisionTypeMask.GetValueAsInt() : COLLISION_NPC_PATROL_CUE_INDEX_PREFIX;

                        var newPatrolCue = new PatrolCue {
                            Id = (null == id || id.IsEmpty) ? NO_PATROL_CUE_ID : id.GetValueAsInt(),
                            FlAct = (null == flAct || flAct.IsEmpty) ? 0 : (ulong)flAct.GetValueAsInt(),
                            FrAct = (null == frAct || frAct.IsEmpty) ? 0 : (ulong)frAct.GetValueAsInt(),
                            FlCaptureFrames = (null == flCaptureFrames || flCaptureFrames.IsEmpty) ? 0 : (ulong)flCaptureFrames.GetValueAsInt(),
                            FrCaptureFrames = (null == frCaptureFrames || frCaptureFrames.IsEmpty) ? 0 : (ulong)frCaptureFrames.GetValueAsInt(),

                            FdAct = (null == fdAct || fdAct.IsEmpty) ? 0 : (ulong)fdAct.GetValueAsInt(),
                            FuAct = (null == fuAct || fuAct.IsEmpty) ? 0 : (ulong)fuAct.GetValueAsInt(),
                            FdCaptureFrames = (null == fdCaptureFrames || fdCaptureFrames.IsEmpty) ? 0 : (ulong)fdCaptureFrames.GetValueAsInt(),
                            FuCaptureFrames = (null == fuCaptureFrames || fuCaptureFrames.IsEmpty) ? 0 : (ulong)fuCaptureFrames.GetValueAsInt(),
                            CollisionTypeMask = collisionTypeMaskVal
                        };

                        var patrolCueCollider = NewRectCollider(patrolCueCx, patrolCueCy, 2 * defaultPatrolCueRadius, 2 * defaultPatrolCueRadius, 0, 0, 0, 0, 0, 0, maxTouchingCellsCnt, newPatrolCue);
                        staticColliders[staticColliderIdx++] = patrolCueCollider;
                        //Debug.Log(String.Format("newPatrolCue={0} at [X:{1}, Y:{2}]", newPatrolCue, patrolCueCx, patrolCueCy));
                    }
                    break;

                case "TrapStartingPos":
                    foreach (Transform trapChild in child) {
                        var tileObj = trapChild.gameObject.GetComponent<SuperObject>();
                        var tileProps = trapChild.gameObject.GetComponent<SuperCustomProperties>();

                        CustomProperty speciesId, providesHardPushback, providesDamage, providesEscape, providesSlipJump, forcesCrouching, isCompletelyStatic, collisionTypeMask, dirX, dirY, speed, triggerTrackingId, prohibitsWallGrabbing;
                        tileProps.TryGetCustomProperty("speciesId", out speciesId);
                        tileProps.TryGetCustomProperty("providesHardPushback", out providesHardPushback);
                        tileProps.TryGetCustomProperty("providesDamage", out providesDamage);
                        tileProps.TryGetCustomProperty("providesEscape", out providesEscape);
                        tileProps.TryGetCustomProperty("providesSlipJump", out providesSlipJump);
                        tileProps.TryGetCustomProperty("forcesCrouching", out forcesCrouching);
                        tileProps.TryGetCustomProperty("static", out isCompletelyStatic);
                        tileProps.TryGetCustomProperty("dirX", out dirX);
                        tileProps.TryGetCustomProperty("dirY", out dirY);
                        tileProps.TryGetCustomProperty("speed", out speed);
                        tileProps.TryGetCustomProperty("triggerTrackingId", out triggerTrackingId);
                        tileProps.TryGetCustomProperty("prohibitsWallGrabbing", out prohibitsWallGrabbing);

                        int speciesIdVal = speciesId.GetValueAsInt(); // Not checking null or empty for this property because it shouldn't be, and in case it comes empty anyway, this automatically throws an error 
                        bool providesHardPushbackVal = (null != providesHardPushback && !providesHardPushback.IsEmpty && 1 == providesHardPushback.GetValueAsInt()) ? true : false;
                        bool providesDamageVal = (null != providesDamage && !providesDamage.IsEmpty && 1 == providesDamage.GetValueAsInt()) ? true : false;
                        bool providesEscapeVal = (null != providesEscape && !providesEscape.IsEmpty && 1 == providesEscape.GetValueAsInt()) ? true : false;
                        bool providesSlipJumpVal = (null != providesSlipJump && !providesSlipJump.IsEmpty && 1 == providesSlipJump.GetValueAsInt()) ? true : false;
                        bool forcesCrouchingVal = (null != forcesCrouching && !forcesCrouching.IsEmpty && 1 == forcesCrouching.GetValueAsInt()) ? true : false;
                        bool isCompletelyStaticVal = (null != isCompletelyStatic && !isCompletelyStatic.IsEmpty && 1 == isCompletelyStatic.GetValueAsInt()) ? true : false;
                        bool prohibitsWallGrabbingVal = (null != prohibitsWallGrabbing && !prohibitsWallGrabbing.IsEmpty && 1 == prohibitsWallGrabbing.GetValueAsInt()) ? true : false;

                        int dirXVal = (null == dirX || dirX.IsEmpty ? 0 : dirX.GetValueAsInt());
                        int dirYVal = (null == dirY || dirY.IsEmpty ? 0 : dirY.GetValueAsInt());
                        int speedVal = (null == speed || speed.IsEmpty ? 0 : speed.GetValueAsInt());
                        int triggerTrackingIdVal = (null == triggerTrackingId || triggerTrackingId.IsEmpty ? 0 : triggerTrackingId.GetValueAsInt());

                        var trapDirMagSq = dirXVal * dirXVal + dirYVal * dirYVal;
                        var invTrapDirMag = InvSqrt32(trapDirMagSq);
                        var trapSpeedXfac = invTrapDirMag * dirXVal;
                        var trapSpeedYfac = invTrapDirMag * dirYVal;

                        int trapVelX = (int)(trapSpeedXfac * speedVal);
                        int trapVelY = (int)(trapSpeedYfac * speedVal);

                        TrapConfig trapConfig = trapConfigs[speciesIdVal];
                        TrapConfigFromTiled trapConfigFromTiled = new TrapConfigFromTiled {
                            SpeciesId = speciesIdVal,
                            Quota = MAGIC_QUOTA_INFINITE,
                            Speed = speedVal,
                            DirX = dirXVal,
                            DirY = dirYVal,
                            ProhibitsWallGrabbing = prohibitsWallGrabbingVal 
                        };

                        tileProps.TryGetCustomProperty("collisionTypeMask", out collisionTypeMask);
                        ulong collisionTypeMaskVal = (null != collisionTypeMask && !collisionTypeMask.IsEmpty) ? (ulong)collisionTypeMask.GetValueAsInt() : 0;

                        List<TrapColliderAttr> colliderAttrs = new List<TrapColliderAttr>();
                        if (isCompletelyStaticVal) {
                            var (tiledRectCx, tiledRectCy) = (tileObj.m_X + tileObj.m_Width * 0.5f, tileObj.m_Y + tileObj.m_Height * 0.5f);
                            var (rectCx, rectCy) = TiledLayerPositionToCollisionSpacePosition(tiledRectCx, tiledRectCy, spaceOffsetX, spaceOffsetY);
                            var (rectVw, rectVh) = PolygonColliderCtrToVirtualGridPos(tileObj.m_Width, tileObj.m_Height);
                            var (rectCenterVx, rectCenterVy) = PolygonColliderCtrToVirtualGridPos(rectCx, rectCy);

                            Trap trap = new Trap {
                                TrapLocalId = trapLocalId,
                                Config = trapConfig,
                                ConfigFromTiled = trapConfigFromTiled,
                                VirtualGridX = rectCenterVx,
                                VirtualGridY = rectCenterVy,
                                DirX = dirXVal,
                                DirY = dirYVal,
                                VelX = trapVelX,
                                VelY = trapVelY,
                                TriggerTrackingId = triggerTrackingIdVal,
                                IsCompletelyStatic = true
                            };

                            TrapColliderAttr colliderAttr = new TrapColliderAttr {
                                ProvidesDamage = providesDamageVal,
                                ProvidesHardPushback = providesHardPushbackVal,
                                ProvidesEscape = providesEscapeVal,
                                ProvidesSlipJump = providesSlipJumpVal,
                                ForcesCrouching = forcesCrouchingVal,
                                HitboxOffsetX = 0,
                                HitboxOffsetY = 0,
                                HitboxSizeX = rectVw,
                                HitboxSizeY = rectVh,
                                CollisionTypeMask = collisionTypeMaskVal,
                                TrapLocalId = trapLocalId
                            };

                            colliderAttrs.Add(colliderAttr);
                            trapLocalIdToColliderAttrs[trapLocalId] = colliderAttrs;

                            var trapCollider = NewRectCollider(rectCx, rectCy, tileObj.m_Width, tileObj.m_Height, 0, 0, 0, 0, 0, 0, maxTouchingCellsCnt, colliderAttr);

                            completelyStaticTrapColliders.Add(trapCollider);
                            trapList.Add(trap);
                            if (0 != trap.TriggerTrackingId) {
                                triggerTrackingIdToTrapLocalId[trap.TriggerTrackingId] = trap.TrapLocalId;
                            }
                            staticColliders[staticColliderIdx++] = trapCollider;
                            trapLocalId++;
                            // Debug.Log(String.Format("new completely static trap created {0}", trap));
                        } else {
                            var (tiledRectCx, tiledRectCy) = (tileObj.m_X + tileObj.m_Width * 0.5f, tileObj.m_Y - tileObj.m_Height * 0.5f);
                            var (rectCx, rectCy) = TiledLayerPositionToCollisionSpacePosition(tiledRectCx, tiledRectCy, spaceOffsetX, spaceOffsetY);
                            var (rectCenterVx, rectCenterVy) = PolygonColliderCtrToVirtualGridPos(rectCx, rectCy);
                            Trap trap = new Trap {
                                TrapLocalId = trapLocalId,
                                Config = trapConfig,
                                ConfigFromTiled = trapConfigFromTiled,
                                VirtualGridX = rectCenterVx,
                                VirtualGridY = rectCenterVy,
                                DirX = dirXVal,
                                DirY = dirYVal,
                                VelX = trapVelX,
                                VelY = trapVelY,
                                TriggerTrackingId = triggerTrackingIdVal,
                                IsCompletelyStatic = false
                            };
                            var collisionObjs = tileObj.m_SuperTile.m_CollisionObjects;
                            foreach (var collisionObj in collisionObjs) {
                                bool childProvidesHardPushbackVal = false, childProvidesDamageVal = false, childProvidesEscapeVal = false, childProvidesSlipJumpVal = false;
                                foreach (var collisionObjProp in collisionObj.m_CustomProperties) {
                                    if ("providesHardPushback".Equals(collisionObjProp.m_Name)) {
                                        childProvidesHardPushbackVal = (!collisionObjProp.IsEmpty && 1 == collisionObjProp.GetValueAsInt());
                                    }
                                    if ("providesDamage".Equals(collisionObjProp.m_Name)) {
                                        childProvidesDamageVal = (!collisionObjProp.IsEmpty && 1 == collisionObjProp.GetValueAsInt());
                                    }
                                    if ("providesEscape".Equals(collisionObjProp.m_Name)) {
                                        childProvidesEscapeVal = (!collisionObjProp.IsEmpty && 1 == collisionObjProp.GetValueAsInt());
                                    }
                                    if ("providesSlipJump".Equals(collisionObjProp.m_Name)) {
                                        childProvidesSlipJumpVal = (!collisionObjProp.IsEmpty && 1 == collisionObjProp.GetValueAsInt());
                                    }
                                    if ("collisionTypeMask".Equals(collisionObjProp.m_Name) && !collisionObjProp.IsEmpty) {
                                        collisionTypeMaskVal = (ulong)collisionObjProp.GetValueAsInt();
                                    }
                                }

                                // [WARNING] The offset (0, 0) of the tileObj within TSX is the top-left corner, but SuperTiled2Unity converted that to bottom-left corner and reverted y-axis by itself... 
                                var (hitboxOffsetCx, hitboxOffsetCy) = (-tileObj.m_Width * 0.5f + collisionObj.m_Position.x + collisionObj.m_Size.x * 0.5f, collisionObj.m_Position.y - collisionObj.m_Size.y * 0.5f - tileObj.m_Height * 0.5f);
                                var (hitboxOffsetVx, hitboxOffsetVy) = PolygonColliderCtrToVirtualGridPos(hitboxOffsetCx, hitboxOffsetCy);
                                var (hitboxSizeVx, hitboxSizeVy) = PolygonColliderCtrToVirtualGridPos(collisionObj.m_Size.x, collisionObj.m_Size.y);
                                TrapColliderAttr colliderAttr = new TrapColliderAttr {
                                    ProvidesDamage = childProvidesDamageVal,
                                    ProvidesHardPushback = childProvidesHardPushbackVal,
                                    ProvidesEscape = childProvidesEscapeVal,
                                    ProvidesSlipJump = childProvidesSlipJumpVal,
                                    HitboxOffsetX = hitboxOffsetVx,
                                    HitboxOffsetY = hitboxOffsetVy,
                                    HitboxSizeX = hitboxSizeVx,
                                    HitboxSizeY = hitboxSizeVy,
                                    CollisionTypeMask = collisionTypeMaskVal,
                                    TrapLocalId = trapLocalId
                                };
                                colliderAttrs.Add(colliderAttr);
                            }
                            // Sort to avoid any platform-specific uncertainty
                            colliderAttrs.Sort(delegate (TrapColliderAttr lhs, TrapColliderAttr rhs) {
                                return Math.Sign(lhs.TrapLocalId - rhs.TrapLocalId);
                            });
                            trapLocalIdToColliderAttrs[trapLocalId] = colliderAttrs;
                            trapList.Add(trap);
                            if (0 != trap.TriggerTrackingId) {
                                triggerTrackingIdToTrapLocalId[trap.TriggerTrackingId] = trap.TrapLocalId;
                            }
                            trapLocalId++;
                            Destroy(trapChild.gameObject); // [WARNING] It'll be animated by "TrapPrefab" in "applyRoomDownsyncFrame" instead!
                        }
                    }
                    break;
                case "TriggerPos":
                    foreach (Transform triggerChild in child) {
                        var tileObj = triggerChild.gameObject.GetComponent<SuperObject>();
                        var tileProps = triggerChild.gameObject.GetComponent<SuperCustomProperties>();

                        CustomProperty bulletTeamId, chCollisionTeamId, delayedFrames, initVelX, initVelY, quota, recoveryFrames, speciesId, trackingIdList, subCycleTriggerFrames, subCycleQuota, characterSpawnerTimeSeq;
                        tileProps.TryGetCustomProperty("bulletTeamId", out bulletTeamId);
                        tileProps.TryGetCustomProperty("chCollisionTeamId", out chCollisionTeamId);
                        tileProps.TryGetCustomProperty("delayedFrames", out delayedFrames);
                        tileProps.TryGetCustomProperty("initVelX", out initVelX);
                        tileProps.TryGetCustomProperty("initVelY", out initVelY);
                        tileProps.TryGetCustomProperty("quota", out quota);
                        tileProps.TryGetCustomProperty("recoveryFrames", out recoveryFrames);
                        tileProps.TryGetCustomProperty("speciesId", out speciesId);
                        tileProps.TryGetCustomProperty("trackingIdList", out trackingIdList);
                        tileProps.TryGetCustomProperty("subCycleTriggerFrames", out subCycleTriggerFrames);
                        tileProps.TryGetCustomProperty("subCycleQuota", out subCycleQuota);
                        tileProps.TryGetCustomProperty("characterSpawnerTimeSeq", out characterSpawnerTimeSeq);

                        int speciesIdVal = speciesId.GetValueAsInt(); // must have 
                        int bulletTeamIdVal = (null != bulletTeamId && !bulletTeamId.IsEmpty ? bulletTeamId.GetValueAsInt() : 0);
                        int chCollisionTeamIdVal = (null != chCollisionTeamId && !chCollisionTeamId.IsEmpty ? chCollisionTeamId.GetValueAsInt() : 0);
                        int delayedFramesVal = (null != delayedFrames && !delayedFrames.IsEmpty ? delayedFrames.GetValueAsInt() : 0);
                        int initVelXVal = (null != initVelX && !initVelX.IsEmpty ? initVelX.GetValueAsInt() : 0);
                        int initVelYVal = (null != initVelY && !initVelY.IsEmpty ? initVelY.GetValueAsInt() : 0);
                        int quotaVal = (null != quota && !quota.IsEmpty ? quota.GetValueAsInt() : 1);
                        int recoveryFramesVal = (null != recoveryFrames && !recoveryFrames.IsEmpty ? recoveryFrames.GetValueAsInt() : delayedFramesVal+1); // By default we must have "recoveryFramesVal > delayedFramesVal"
                        var trackingIdListStr = (null != trackingIdList && !trackingIdList.IsEmpty ? trackingIdList.GetValueAsString() : "");
                        int subCycleTriggerFramesVal = (null != subCycleTriggerFrames && !subCycleTriggerFrames.IsEmpty ? subCycleTriggerFrames.GetValueAsInt() : 0);
                        int subCycleQuotaVal = (null != subCycleQuota && !subCycleQuota.IsEmpty ? subCycleQuota.GetValueAsInt() : 0);
                        var characterSpawnerTimeSeqStr = (null != characterSpawnerTimeSeq && !characterSpawnerTimeSeq.IsEmpty ? characterSpawnerTimeSeq.GetValueAsString() : "");

                        var triggerConfig = triggerConfigs[speciesIdVal];
                        var trigger = new Trigger {
                            TriggerLocalId = triggerLocalId,
                            BulletTeamId = bulletTeamIdVal,
                            Quota = (TRIGGER_MASK_BY_CYCLIC_TIMER == triggerConfig.TriggerMask 
                                    ? 
                                    quotaVal - 1 // The first quota will be spent right at "delayedFramesVal"
                                    :
                                    quotaVal),
                            State = TriggerState.Tready,
                            SubCycleQuotaLeft = subCycleQuotaVal,
                            FramesToFire = (TRIGGER_MASK_BY_CYCLIC_TIMER == triggerConfig.TriggerMask ? delayedFramesVal : MAX_INT),
                            FramesToRecover = (TRIGGER_MASK_BY_CYCLIC_TIMER == triggerConfig.TriggerMask ? delayedFramesVal+recoveryFramesVal : 0),
                            Config = triggerConfig,
                            ConfigFromTiled = new TriggerConfigFromTiled {
                                SpeciesId = speciesIdVal,
                                ChCollisionTeamId = chCollisionTeamIdVal,
                                DelayedFrames = delayedFramesVal,
                                RecoveryFrames = recoveryFramesVal,
                                InitVelX = initVelXVal,
                                InitVelY = initVelYVal,
                                SubCycleTriggerFrames = subCycleTriggerFramesVal,
                                SubCycleQuota = subCycleQuotaVal,
                            },
                        };

                        string[] trackingIdListStrParts = trackingIdListStr.Split(',');
                        foreach (var trackingIdListStrPart in trackingIdListStrParts) {
                            if (String.IsNullOrEmpty(trackingIdListStrPart)) continue;
                            trigger.ConfigFromTiled.TrackingIdList.Add(trackingIdListStrPart.ToInt());
                        }
                        string[] characterSpawnerTimeSeqStrParts = characterSpawnerTimeSeqStr.Split(';');
                        foreach (var part in characterSpawnerTimeSeqStrParts) {
                            if (String.IsNullOrEmpty(part)) continue;
                            string[] subParts = part.Split(':');
                            if (2 != subParts.Length) continue;
                            if (String.IsNullOrEmpty(subParts[0])) continue;
                            if (String.IsNullOrEmpty(subParts[1])) continue;
                            int cutoffRdfFrameId = subParts[0].ToInt(); 
                            var chSpawnerConfig = new CharacterSpawnerConfig{
                                CutoffRdfFrameId = cutoffRdfFrameId
                            };
                            string[] speciesIdParts = subParts[1].Split(','); 
                            foreach (var speciesIdPart in speciesIdParts) {
                                chSpawnerConfig.SpeciesIdList.Add(speciesIdPart.ToInt());
                            }
                            trigger.ConfigFromTiled.CharacterSpawnerTimeSeq.Add(chSpawnerConfig);
                        }
                        var (tiledRectCx, tiledRectCy) = (tileObj.m_X + tileObj.m_Width * 0.5f, tileObj.m_Y - tileObj.m_Height * 0.5f);
                        var (rectCx, rectCy) = TiledLayerPositionToCollisionSpacePosition(tiledRectCx, tiledRectCy, spaceOffsetX, spaceOffsetY);
                        var (rectCenterVx, rectCenterVy) = PolygonColliderCtrToVirtualGridPos(rectCx, rectCy);
                        trigger.VirtualGridX = rectCenterVx;
                        trigger.VirtualGridY = rectCenterVy;
                        var (wx, wy) = CollisionSpacePositionToWorldPosition(rectCx, rectCy, spaceOffsetX, spaceOffsetY);
                        triggerList.Add((trigger, wx, wy));
                    
                        var triggerCollderAttr = new TriggerColliderAttr {
                           TriggerLocalId = triggerLocalId
                        };
                        var triggerCollider = NewRectCollider(rectCx, rectCy, tileObj.m_Width, tileObj.m_Height, 0, 0, 0, 0, 0, 0, maxTouchingCellsCnt, triggerCollderAttr);
                        staticColliders[staticColliderIdx++] = triggerCollider;
                        ++triggerLocalId;
                        Destroy(triggerChild.gameObject); // [WARNING] It'll be animated by "TriggerPrefab" in "applyRoomDownsyncFrame" instead!
                    }
                    break;
                default:
                    break;
            }
        }

        // Sorting to make sure that if "roomCapacity" is smaller than the position counts in Tiled, we take only the smaller teamIds
        playerStartingCposList.Sort(delegate ((Vector, int, int) lhs, (Vector, int, int) rhs) {
            return Math.Sign(lhs.Item2 - rhs.Item2);
        });

        for (int i = 0; i < staticColliderIdx; i++) {
            collisionSys.AddSingle(staticColliders[i]);
        }

        var startRdf = NewPreallocatedRoomDownsyncFrame(roomCapacity, preallocNpcCapacity, preallocBulletCapacity, preallocTrapCapacity, preallocTriggerCapacity);
        historyRdfHolder = NewPreallocatedRoomDownsyncFrame(roomCapacity, preallocNpcCapacity, preallocBulletCapacity, preallocTrapCapacity, preallocTriggerCapacity);

        startRdf.Id = DOWNSYNC_MSG_ACT_BATTLE_READY_TO_START;
        startRdf.ShouldForceResync = false;
        for (int i = 0; i < roomCapacity; i++) {
            int joinIndex = i + 1;
            var (cpos, teamId, dirX) = playerStartingCposList[i];
            var (wx, wy) = CollisionSpacePositionToWorldPosition(cpos.X, cpos.Y, spaceOffsetX, spaceOffsetY);
            teamId = (DEFAULT_BULLET_TEAM_ID == teamId ? joinIndex : teamId);
            var playerInRdf = startRdf.PlayersArr[i];
            playerInRdf.SpeciesId = speciesIdList[i];
            playerInRdf.JoinIndex = joinIndex;
            playerInRdf.BulletTeamId = teamId;
            playerInRdf.ChCollisionTeamId = teamId; // If we want to stand on certain teammates' shoulder, then this value should be tuned accordingly. 
            var chConfig = Battle.characters[playerInRdf.SpeciesId];
            spawnPlayerNode(joinIndex, playerInRdf.SpeciesId, wx, wy, playerInRdf.BulletTeamId);
            var (playerVposX, playerVposY) = PolygonColliderCtrToVirtualGridPos(cpos.X, cpos.Y); // World and CollisionSpace coordinates have the same scale, just translated
            playerInRdf.VirtualGridX = playerVposX;
            playerInRdf.VirtualGridY = playerVposY;
            playerInRdf.RevivalVirtualGridX = playerVposX;
            playerInRdf.RevivalVirtualGridY = playerVposY;
            playerInRdf.RevivalDirX = dirX;
            playerInRdf.RevivalDirY = 0;
            playerInRdf.Speed = chConfig.Speed;
            playerInRdf.CharacterState = CharacterState.InAirIdle1NoJump;
            playerInRdf.FramesToRecover = 0;
            playerInRdf.DirX = dirX;
            playerInRdf.DirY = 0;
            playerInRdf.VelX = 0;
            playerInRdf.VelY = 0;
            playerInRdf.InAir = true;
            playerInRdf.OnWall = false;
            playerInRdf.Hp = chConfig.Hp;
            playerInRdf.MaxHp = chConfig.Hp;
            playerInRdf.Mp = 1000;
            playerInRdf.MaxMp = 1000;
            playerInRdf.OmitGravity = chConfig.OmitGravity;
            playerInRdf.OmitSoftPushback = chConfig.OmitSoftPushback;
            playerInRdf.RepelSoftPushback = chConfig.RepelSoftPushback;
            playerInRdf.CollisionTypeMask = COLLISION_CHARACTER_INDEX_PREFIX;

            // TODO: Remove the hardcoded index
            var initIvSlot = chConfig.InitInventorySlots[0];
            AssignToInventorySlot(initIvSlot.StockType, initIvSlot.Quota, initIvSlot.FramesToRecover, initIvSlot.DefaultQuota, initIvSlot.DefaultFramesToRecover, initIvSlot.BuffConfig, playerInRdf.Inventory.Slots[0]);
        }

        int npcLocalId = 0;
        for (int i = 0; i < npcsStartingCposList.Count; i++) {
            int joinIndex = roomCapacity + i + 1;
            var (cpos, dirX, dirY, characterSpeciesId, teamId, isStatic) = npcsStartingCposList[i];
            var (wx, wy) = CollisionSpacePositionToWorldPosition(cpos.X, cpos.Y, spaceOffsetX, spaceOffsetY);
            var chConfig = Battle.characters[characterSpeciesId];
            var npcInRdf = startRdf.NpcsArr[i];
            var (vx, vy) = PolygonColliderCtrToVirtualGridPos(cpos.X, cpos.Y);
            npcInRdf.Id = npcLocalId; // Just for not being excluded 
            npcInRdf.JoinIndex = joinIndex;
            npcInRdf.VirtualGridX = vx;
            npcInRdf.VirtualGridY = vy;
            npcInRdf.RevivalVirtualGridX = vx;
            npcInRdf.RevivalVirtualGridY = vy;
            npcInRdf.RevivalDirX = dirX;
            npcInRdf.RevivalDirY = dirY;
            npcInRdf.Speed = chConfig.Speed;
            npcInRdf.CharacterState = CharacterState.InAirIdle1NoJump;
            npcInRdf.FramesToRecover = 0;
            npcInRdf.DirX = dirX;
            npcInRdf.DirY = dirY;
            npcInRdf.VelX = 0;
            npcInRdf.VelY = 0;
            npcInRdf.InAir = true;
            npcInRdf.OnWall = false;
            npcInRdf.Hp = chConfig.Hp;
            npcInRdf.MaxHp = chConfig.Hp;
            npcInRdf.Mp = 1000;
            npcInRdf.MaxMp = 1000;
            npcInRdf.SpeciesId = characterSpeciesId;
            npcInRdf.BulletTeamId = teamId;
            npcInRdf.ChCollisionTeamId = teamId;
            npcInRdf.CollisionTypeMask = COLLISION_CHARACTER_INDEX_PREFIX;
            npcInRdf.WaivingSpontaneousPatrol = isStatic;
            npcInRdf.OmitGravity = chConfig.OmitGravity;
            npcInRdf.OmitSoftPushback = chConfig.OmitSoftPushback;
            npcInRdf.RepelSoftPushback = chConfig.RepelSoftPushback;
            startRdf.NpcsArr[i] = npcInRdf;
            npcLocalId++;
        }
        startRdf.NpcLocalIdCounter = npcLocalId;

        for (int i = 0; i < trapList.Count; i++) {
            var trap = trapList[i];
            startRdf.TrapsArr[i] = trap;
            if (trap.IsCompletelyStatic) continue;
            var (cx, cy) = VirtualGridToPolygonColliderCtr(trap.VirtualGridX, trap.VirtualGridY);
            var (wx, wy) = CollisionSpacePositionToWorldPosition(cx, cy, spaceOffsetX, spaceOffsetY);
            spawnDynamicTrapNode(trap.Config.SpeciesId, wx, wy);
        }

        for (int i = 0; i < triggerList.Count; i++) {
            var (trigger, wx, wy) = triggerList[i];
            startRdf.TriggersArr[i] = trigger;
            spawnTriggerNode(trigger.TriggerLocalId, trigger.Config.SpeciesId, wx, wy);
        }

        return startRdf;
    }

    protected void popupErrStackPanel(string msg) {
        if (null == errStackLogPanelObj) {
            errStackLogPanelObj = Instantiate(errStackLogPanelPrefab, new Vector3(canvas.transform.position.x, canvas.transform.position.y, +5), Quaternion.identity, canvas.transform);
        }
        var errStackLogPanel = errStackLogPanelObj.GetComponent<ErrStackLogPanel>();
        errStackLogPanel.content.text = msg;
    }


    protected Vector2 camSpeedHolder = new Vector2();
    protected Vector2 camDiffDstHolder = new Vector2();
    protected void cameraTrack(RoomDownsyncFrame rdf, RoomDownsyncFrame prevRdf) {
        if (null == selfPlayerInfo) return;
        int targetJoinIndex = isBattleResultSet(confirmedBattleResult) ? confirmedBattleResult.WinnerJoinIndex : selfPlayerInfo.JoinIndex;

        var playerGameObj = playerGameObjs[targetJoinIndex - 1];
        var playerCharacterDownsync = rdf.PlayersArr[targetJoinIndex - 1];

        var (velCX, velCY) = VirtualGridToPolygonColliderCtr(playerCharacterDownsync.Speed, playerCharacterDownsync.Speed);
        camSpeedHolder.Set(velCX, velCY);
        var cameraSpeedInWorld = camSpeedHolder.magnitude * 100;

        var prevPlayerCharacterDownsync = prevRdf.PlayersArr[targetJoinIndex - 1];
        if (CharacterState.Dying == prevPlayerCharacterDownsync.CharacterState || isBattleResultSet(confirmedBattleResult)) {
            cameraSpeedInWorld *= 200;
        }

        var camOldPos = Camera.main.transform.position;
        var dst = playerGameObj.transform.position;
        camDiffDstHolder.Set(dst.x - camOldPos.x, dst.y - camOldPos.y);

        //Debug.Log(String.Format("cameraTrack, camOldPos={0}, dst={1}, deltaTime={2}", camOldPos, dst, Time.deltaTime));
        var stepLength = Time.deltaTime * cameraSpeedInWorld;
        if (stepLength > camDiffDstHolder.magnitude) {
            newPosHolder.Set(dst.x, dst.y, camOldPos.z);
            Camera.main.transform.position = newPosHolder;
        } else {
            var newMapPosDiff2 = camDiffDstHolder.normalized * stepLength;
            newPosHolder.Set(camOldPos.x + newMapPosDiff2.x, camOldPos.y + newMapPosDiff2.y, camOldPos.z);
            Camera.main.transform.position = newPosHolder;
        }
    }

    protected void resetLine(DebugLine line) {
        newPosHolder.x = 0;
        newPosHolder.y = 0;
        line.transform.position = newPosHolder;
        line.GetPositions(debugDrawPositionsHolder);
        (debugDrawPositionsHolder[0].x, debugDrawPositionsHolder[0].y) = (0, 0);
        (debugDrawPositionsHolder[1].x, debugDrawPositionsHolder[1].y) = (0, 0);
        (debugDrawPositionsHolder[2].x, debugDrawPositionsHolder[2].y) = (0, 0);
        (debugDrawPositionsHolder[3].x, debugDrawPositionsHolder[3].y) = (0, 0);
        line.SetPositions(debugDrawPositionsHolder);
    }

    public void toggleDebugDrawingEnabled() {
        debugDrawingEnabled = !debugDrawingEnabled; 
    }

    protected void urpDrawDebug() {
        if (ROOM_STATE_IN_BATTLE != battleState) {
            return;
        }
        for (int i = cachedLineRenderers.vals.StFrameId; i < cachedLineRenderers.vals.EdFrameId; i++) {
            var (res, line) = cachedLineRenderers.vals.GetByFrameId(i);
            if (!res || null == line) throw new ArgumentNullException(String.Format("There's no line for i={0}, while StFrameId={1}, EdFrameId={2}", i, cachedLineRenderers.vals.StFrameId, cachedLineRenderers.vals.EdFrameId));

            resetLine(line);
        }
        if (!debugDrawingEnabled) {
            return;
        }
        var (_, rdf) = renderBuffer.GetByFrameId(playerRdfId);
        if (null == rdf) return;

        // Draw static colliders
        int lineIndex = 0;
        foreach (var collider in staticColliders) {
            if (null == collider) {
                break;
            }
            if (null == collider.Shape) {
                throw new ArgumentNullException("barrierCollider.Shape is null when drawing staticRectangleColliders");
            }
            if (null == collider.Shape.Points) {
                throw new ArgumentNullException("barrierCollider.Shape.Points is null when drawing staticRectangleColliders");
            }

            var (wx, wy) = CollisionSpacePositionToWorldPosition(collider.X, collider.Y, spaceOffsetX, spaceOffsetY);;
            newPosHolder.Set(wx, wy, 0);
            if (!isGameObjPositionWithinCamera(newPosHolder)) {
                continue; // To save memory
            }

            string key = "Static-" + lineIndex.ToString();
            lineIndex++;
            var line = cachedLineRenderers.PopAny(key);
            if (null == line) {
                line = cachedLineRenderers.Pop();
            }
            if (null == line) {
                throw new ArgumentNullException("Cached line is null for key:" + key);
            }
            line.SetColor(Color.white);
            if (null != collider.Data) {
#nullable enable
                TrapColliderAttr? trapColliderAttr = collider.Data as TrapColliderAttr;
                if (null != trapColliderAttr) {
                    if (trapColliderAttr.ProvidesHardPushback) {
                        line.SetColor(Color.green);
                    } else if (trapColliderAttr.ProvidesDamage) {
                        line.SetColor(Color.red);
                    }
                } else {
                    TriggerColliderAttr? triggerColliderAttr = collider.Data as TriggerColliderAttr;
                    if (null != triggerColliderAttr) {
                        var trigger = rdf.TriggersArr[triggerColliderAttr.TriggerLocalId];
                        if (0 < (TRIGGER_MASK_BY_MOVEMENT & trigger.Config.TriggerMask)) {
                            line.SetColor(Color.magenta);
                        } else if (0 < (TRIGGER_MASK_BY_ATK & trigger.Config.TriggerMask)) {
                            line.SetColor(Color.cyan);
                        }
                    }
                }
#nullable disable
            }
            int m = collider.Shape.Points.Cnt;
            line.GetPositions(debugDrawPositionsHolder);
            for (int i = 0; i < m; i++) {
                var (_, pi) = collider.Shape.Points.GetByOffset(i);
                (debugDrawPositionsHolder[i].x, debugDrawPositionsHolder[i].y) = CollisionSpacePositionToWorldPosition(collider.X + pi.X, collider.Y + pi.Y, spaceOffsetX, spaceOffsetY);
            }
            line.SetPositions(debugDrawPositionsHolder);
            line.score = rdf.Id;
            cachedLineRenderers.Put(key, line);
        }

        // Draw dynamic colliders
        for (int k = 0; k < roomCapacity; k++) {
            var currCharacterDownsync = rdf.PlayersArr[k];
            var chConfig = characters[currCharacterDownsync.SpeciesId];
            float boxCx, boxCy, boxCw, boxCh;
            calcCharacterBoundingBoxInCollisionSpace(currCharacterDownsync, chConfig, currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY, out boxCx, out boxCy, out boxCw, out boxCh);
            var (wx, wy) = CollisionSpacePositionToWorldPosition(boxCx, boxCy, spaceOffsetX, spaceOffsetY);
            newPosHolder.Set(wx, wy, 0);
            if (!isGameObjPositionWithinCamera(newPosHolder)) {
                continue; // To save memory
            }

            string key = "Player-" + currCharacterDownsync.JoinIndex.ToString();
            var line = cachedLineRenderers.PopAny(key);
            if (null == line) {
                line = cachedLineRenderers.Pop();
            }
            if (null == line) {
                throw new ArgumentNullException("Cached line is null for key:" + key);
            }
            line.SetColor(Color.white);
            line.GetPositions(debugDrawPositionsHolder);
           
            // World space width and height are just the same as that of collision space.

            (debugDrawPositionsHolder[0].x, debugDrawPositionsHolder[0].y) = ((wx - 0.5f * boxCw), (wy - 0.5f * boxCh));
            (debugDrawPositionsHolder[1].x, debugDrawPositionsHolder[1].y) = ((wx + 0.5f * boxCw), (wy - 0.5f * boxCh));
            (debugDrawPositionsHolder[2].x, debugDrawPositionsHolder[2].y) = ((wx + 0.5f * boxCw), (wy + 0.5f * boxCh));
            (debugDrawPositionsHolder[3].x, debugDrawPositionsHolder[3].y) = ((wx - 0.5f * boxCw), (wy + 0.5f * boxCh));
            line.SetPositions(debugDrawPositionsHolder);
            line.score = rdf.Id;
            cachedLineRenderers.Put(key, line);
        }

        for (int k = 0; k < rdf.NpcsArr.Count; k++) {
            var currCharacterDownsync = rdf.NpcsArr[k];
            if (TERMINATING_PLAYER_ID == currCharacterDownsync.Id) break;
            var chConfig = characters[currCharacterDownsync.SpeciesId];
            float boxCx, boxCy, boxCw, boxCh;
            calcCharacterBoundingBoxInCollisionSpace(currCharacterDownsync, chConfig, currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY, out boxCx, out boxCy, out boxCw, out boxCh);

            var (wx, wy) = CollisionSpacePositionToWorldPosition(boxCx, boxCy, spaceOffsetX, spaceOffsetY);
            newPosHolder.Set(wx, wy, 0);
            if (!isGameObjPositionWithinCamera(newPosHolder)) {
                continue; // To save memory
            }

            string key = "Npc-" + currCharacterDownsync.JoinIndex.ToString();
            var line = cachedLineRenderers.PopAny(key);
            if (null == line) {
                line = cachedLineRenderers.Pop();
            }
            if (null == line) {
                throw new ArgumentNullException("Cached line is null for key:" + key);
            }
            line.SetColor(Color.gray);
            line.GetPositions(debugDrawPositionsHolder);
           
            (debugDrawPositionsHolder[0].x, debugDrawPositionsHolder[0].y) = ((wx - 0.5f * boxCw), (wy - 0.5f * boxCh));
            (debugDrawPositionsHolder[1].x, debugDrawPositionsHolder[1].y) = ((wx + 0.5f * boxCw), (wy - 0.5f * boxCh));
            (debugDrawPositionsHolder[2].x, debugDrawPositionsHolder[2].y) = ((wx + 0.5f * boxCw), (wy + 0.5f * boxCh));
            (debugDrawPositionsHolder[3].x, debugDrawPositionsHolder[3].y) = ((wx - 0.5f * boxCw), (wy + 0.5f * boxCh));
            line.SetPositions(debugDrawPositionsHolder);
            line.score = rdf.Id;
            cachedLineRenderers.Put(key, line);

            string keyVision = "NpcVision-" + currCharacterDownsync.JoinIndex.ToString();
            var lineVision = cachedLineRenderers.PopAny(keyVision);
            if (null == lineVision) {
                lineVision = cachedLineRenderers.Pop();
            }
            if (null == lineVision) {
                throw new ArgumentNullException("Cached line is null for keyVision:" + keyVision);
            }
            lineVision.SetColor(Color.yellow);
            lineVision.GetPositions(debugDrawPositionsHolder);
            float visionCx, visionCy, visionCw, visionCh;
            calcNpcVisionBoxInCollisionSpace(currCharacterDownsync, chConfig, out visionCx, out visionCy, out visionCw, out visionCh);
            (wx, wy) = CollisionSpacePositionToWorldPosition(visionCx, visionCy, spaceOffsetX, spaceOffsetY);

            (debugDrawPositionsHolder[0].x, debugDrawPositionsHolder[0].y) = ((wx - 0.5f * visionCw), (wy - 0.5f * visionCh));
            (debugDrawPositionsHolder[1].x, debugDrawPositionsHolder[1].y) = ((wx + 0.5f * visionCw), (wy - 0.5f * visionCh));
            (debugDrawPositionsHolder[2].x, debugDrawPositionsHolder[2].y) = ((wx + 0.5f * visionCw), (wy + 0.5f * visionCh));
            (debugDrawPositionsHolder[3].x, debugDrawPositionsHolder[3].y) = ((wx - 0.5f * visionCw), (wy + 0.5f * visionCh));
            lineVision.SetPositions(debugDrawPositionsHolder);
            lineVision.score = rdf.Id;
            cachedLineRenderers.Put(keyVision, lineVision);
        }

        for (int k = 0; k < rdf.Bullets.Count; k++) {
            var bullet = rdf.Bullets[k];
            if (TERMINATING_BULLET_LOCAL_ID == bullet.BattleAttr.BulletLocalId) break;
            var (cx, cy) = VirtualGridToPolygonColliderCtr(bullet.VirtualGridX, bullet.VirtualGridY);
            var (wx, wy) = CollisionSpacePositionToWorldPosition(cx, cy, spaceOffsetX, spaceOffsetY);;
            newPosHolder.Set(wx, wy, 0);
            if (!isGameObjPositionWithinCamera(newPosHolder)) {
                continue; // To save memory
            }

            string key = "Bullet-" + bullet.BattleAttr.BulletLocalId.ToString();
            var line = cachedLineRenderers.PopAny(key);
            if (null == line) {
                line = cachedLineRenderers.Pop();
            }
            if (null == line) {
                throw new ArgumentNullException("Cached line is null for key:" + key);
            }
            if (!IsBulletActive(bullet, rdf.Id)) {
                cachedLineRenderers.Put(key, line);
                continue;
            }
            line.SetColor(Color.red);
            line.GetPositions(debugDrawPositionsHolder);

            var (boxCw, boxCh) = VirtualGridToPolygonColliderCtr(bullet.Config.HitboxSizeX + bullet.Config.HitboxSizeIncX*bullet.FramesInBlState, bullet.Config.HitboxSizeY + bullet.Config.HitboxSizeIncY * bullet.FramesInBlState);
            (debugDrawPositionsHolder[0].x, debugDrawPositionsHolder[0].y) = ((wx - 0.5f * boxCw), (wy - 0.5f * boxCh));
            (debugDrawPositionsHolder[1].x, debugDrawPositionsHolder[1].y) = ((wx + 0.5f * boxCw), (wy - 0.5f * boxCh));
            (debugDrawPositionsHolder[2].x, debugDrawPositionsHolder[2].y) = ((wx + 0.5f * boxCw), (wy + 0.5f * boxCh));
            (debugDrawPositionsHolder[3].x, debugDrawPositionsHolder[3].y) = ((wx - 0.5f * boxCw), (wy + 0.5f * boxCh));

            // Debug.Log("Active Bullet " + bullet.BattleAttr.BulletLocalId.ToString() + ": wx=" + wx.ToString() + ", wy=" + wy.ToString() + ", boxCw=" + boxCw.ToString() + ", boxCh=" + boxCh.ToString());
            line.SetPositions(debugDrawPositionsHolder);
            line.score = rdf.Id;
            cachedLineRenderers.Put(key, line);
        }

        for (int i = 0; i < rdf.TrapsArr.Count; i++) {
            var currTrap = rdf.TrapsArr[i];
            if (TERMINATING_TRAP_ID == currTrap.TrapLocalId) continue;
            if (currTrap.IsCompletelyStatic) continue;

            List<TrapColliderAttr> colliderAttrs = trapLocalIdToColliderAttrs[currTrap.TrapLocalId];
            foreach (var colliderAttr in colliderAttrs) {
                float boxCx, boxCy, boxCw, boxCh;
                calcTrapBoxInCollisionSpace(colliderAttr, currTrap.VirtualGridX, currTrap.VirtualGridY, out boxCx, out boxCy, out boxCw, out boxCh);
                var (wx, wy) = CollisionSpacePositionToWorldPosition(boxCx, boxCy, spaceOffsetX, spaceOffsetY);
                newPosHolder.Set(wx, wy, 0);
                if (!isGameObjPositionWithinCamera(newPosHolder)) {
                    continue; // To save memory
                }

                string key = "DynamicTrap-" + currTrap.TrapLocalId.ToString() + "-" + colliderAttr.ProvidesDamage; // TODO: Use a collider ID for the last part
                var line = cachedLineRenderers.PopAny(key);
                if (null == line) {
                    line = cachedLineRenderers.Pop();
                }
                if (null == line) {
                    throw new ArgumentNullException("Cached line is null for key:" + key);
                }
                if (colliderAttr.ProvidesHardPushback) {
                    line.SetColor(Color.green);
                } else if (colliderAttr.ProvidesDamage) {
                    line.SetColor(Color.red);
                }

                line.GetPositions(debugDrawPositionsHolder);
                
                (debugDrawPositionsHolder[0].x, debugDrawPositionsHolder[0].y) = ((wx - 0.5f * boxCw), (wy - 0.5f * boxCh));
                (debugDrawPositionsHolder[1].x, debugDrawPositionsHolder[1].y) = ((wx + 0.5f * boxCw), (wy - 0.5f * boxCh));
                (debugDrawPositionsHolder[2].x, debugDrawPositionsHolder[2].y) = ((wx + 0.5f * boxCw), (wy + 0.5f * boxCh));
                (debugDrawPositionsHolder[3].x, debugDrawPositionsHolder[3].y) = ((wx - 0.5f * boxCw), (wy + 0.5f * boxCh));

                line.SetPositions(debugDrawPositionsHolder);
                line.score = rdf.Id;
                cachedLineRenderers.Put(key, line);
            }
        }
    }

    public bool isGameObjPositionWithinCamera(Vector3 positionHolder) {
        var posInMainCamViewport = Camera.main.WorldToViewportPoint(positionHolder);
        return (0f <= posInMainCamViewport.x && posInMainCamViewport.x <= 1f && 0f <= posInMainCamViewport.y && posInMainCamViewport.y <= 1f && 0f < posInMainCamViewport.z);
    }

    public bool isGameObjWithinCamera(GameObject obj) {
        return isGameObjPositionWithinCamera(obj.transform.position);
    }

    public void showTeamRibbonAndInplaceHpBar(int rdfId, CharacterDownsync currCharacterDownsync, float wx, float wy, float halfBoxCw, float halfBoxCh, string lookupKey) {
        var teamRibbon = cachedTeamRibbons.PopAny(lookupKey);
        if (null == teamRibbon) {
            teamRibbon = cachedTeamRibbons.Pop();
        }

        if (null == teamRibbon) {
            throw new ArgumentNullException(String.Format("No available teamRibbon node for lookupKey={0}", lookupKey));
        }

        newPosHolder.Set(wx + teamRibbonOffset.x, wy + halfBoxCh + teamRibbonOffset.y, inplaceHpBarZ);
        teamRibbon.gameObject.transform.position = newPosHolder;
        teamRibbon.score = rdfId;
        teamRibbon.setBulletTeamId(currCharacterDownsync.BulletTeamId);
        cachedTeamRibbons.Put(lookupKey, teamRibbon);

        var hpBar = cachedHpBars.PopAny(lookupKey);
        if (null == hpBar) {
            hpBar = cachedHpBars.Pop();
        }

        if (null == hpBar) {
            throw new ArgumentNullException(String.Format("No available hpBar node for lookupKey={0}", lookupKey));
        }
        hpBar.score = rdfId;
        hpBar.updateHp((float)currCharacterDownsync.Hp / currCharacterDownsync.MaxHp, (float)currCharacterDownsync.Mp / currCharacterDownsync.MaxMp);
        newPosHolder.Set(wx + inplaceHpBarOffset.x, wy + halfBoxCh + inplaceHpBarOffset.y, inplaceHpBarZ);
        hpBar.gameObject.transform.position = newPosHolder;
        cachedHpBars.Put(lookupKey, hpBar);
    }

    public bool playCharacterDamagedVfx(CharacterDownsync currCharacterDownsync, CharacterConfig chConfig, CharacterDownsync prevCharacterDownsync, GameObject theGameObj, CharacterAnimController chAnimCtrl) {
        var spr = theGameObj.GetComponent<SpriteRenderer>();
        var material = spr.material;
        material.SetFloat("_CrackOpacity", 0f);

        if (null != currCharacterDownsync.DebuffList) {
            for (int i = 0; i < currCharacterDownsync.DebuffList.Count; i++) {
                Debuff debuff = currCharacterDownsync.DebuffList[i];
                if (TERMINATING_DEBUFF_SPECIES_ID == debuff.SpeciesId) break;
                switch (debuff.DebuffConfig.Type) {
                    case DebuffType.FrozenPositionLocked:
                        if (0 < debuff.Stock) {
                            material.SetFloat("_CrackOpacity", 0.75f);
                            CharacterState overwriteChState = currCharacterDownsync.CharacterState;
                            if (!noOpSet.Contains(overwriteChState)) {
                                overwriteChState = CharacterState.Atked1;
                            }
                            if (SPECIES_GUNGIRL == currCharacterDownsync.SpeciesId) {
                                chAnimCtrl.updateTwoPartsCharacterAnim(currCharacterDownsync, overwriteChState, prevCharacterDownsync, false, chConfig, effectivelyInfinitelyFar);
                            } else {
                                chAnimCtrl.updateCharacterAnim(currCharacterDownsync, overwriteChState, prevCharacterDownsync, false, chConfig);
                            }
                        }
                        break;
                }
            }
        }

        if (
            null == prevCharacterDownsync 
            || prevCharacterDownsync.Hp <= currCharacterDownsync.Hp 
            || prevCharacterDownsync.Id != currCharacterDownsync.Id // for left-shifted NPCs
        ) return false;
        
        // Some characters, and almost all traps wouldn't have an "attacked state", hence showing their damaged animation by shader.
        /*
        DOTween.Sequence().Append(
            DOTween.To(() => material.GetFloat(MATERIAL_REF_THICKNESS), x => material.SetFloat(MATERIAL_REF_THICKNESS, x), DAMAGED_THICKNESS, DAMAGED_BLINK_SECONDS_HALF))
            .Append(DOTween.To(() => material.GetFloat(MATERIAL_REF_THICKNESS), x => material.SetFloat(MATERIAL_REF_THICKNESS, x), 0f, DAMAGED_BLINK_SECONDS_HALF));
        */
        DOTween.Sequence().Append(
            DOTween.To(() => material.GetFloat(MATERIAL_REF_FLASH_INTENSITY), x => material.SetFloat(MATERIAL_REF_FLASH_INTENSITY, x), DAMAGED_FLASH_INTENSITY, DAMAGED_BLINK_SECONDS_HALF))
            .Append(DOTween.To(() => material.GetFloat(MATERIAL_REF_FLASH_INTENSITY), x => material.SetFloat(MATERIAL_REF_FLASH_INTENSITY, x), 0f, DAMAGED_BLINK_SECONDS_HALF));
        
        return true;
    }

    public bool playCharacterSfx(CharacterDownsync currCharacterDownsync, CharacterDownsync prevCharacterDownsync, CharacterConfig chConfig, float wx, float wy, int rdfId, float distanceAttenuationZ) {
        // Cope with footstep sounds first
        if (CharacterState.Walking == currCharacterDownsync.CharacterState || CharacterState.WalkingAtk1 == currCharacterDownsync.CharacterState) {
            bool usingSameAudSrc = true;
            string ftSfxLookupKey = "ch-ft-" + currCharacterDownsync.JoinIndex.ToString();
            var ftSfxSourceHolder = cachedSfxNodes.PopAny(ftSfxLookupKey);
            if (null == ftSfxSourceHolder) {
                ftSfxSourceHolder = cachedSfxNodes.Pop();
                usingSameAudSrc = false;
            }

            if (null == ftSfxSourceHolder) {
                return false;
                // throw new ArgumentNullException(String.Format("No available ftSfxSourceHolder node for ftSfxLookupKey={0}", ftSfxLookupKey));
            }

            try {
                var clipName = calcFootStepSfxName(currCharacterDownsync); 
                if (null == clipName) {
                    return false;
                }
                if (!ftSfxSourceHolder.audioClipDict.ContainsKey(clipName)) {
                    return false;
                }

                float totAttZ = distanceAttenuationZ + footstepAttenuationZ;
                newPosHolder.Set(wx, wy, totAttZ);
                ftSfxSourceHolder.gameObject.transform.position = newPosHolder;
                if (!usingSameAudSrc || !ftSfxSourceHolder.audioSource.isPlaying) { 
                    ftSfxSourceHolder.audioSource.volume = calcSfxVolume(ftSfxSourceHolder, totAttZ);
                    ftSfxSourceHolder.audioSource.PlayOneShot(ftSfxSourceHolder.audioClipDict[clipName]);
                }
                ftSfxSourceHolder.score = rdfId;
            } finally {
                cachedSfxNodes.Put(ftSfxLookupKey, ftSfxSourceHolder);
            }
        }

        bool isInitialFrame = (0 == currCharacterDownsync.FramesInChState);
        if (!isInitialFrame) {
            return false;
        }

        if (!skills.ContainsKey(currCharacterDownsync.ActiveSkillId)) return false;
        var currSkillConfig = skills[currCharacterDownsync.ActiveSkillId];
        var currBulletConfig = currSkillConfig.Hits[currCharacterDownsync.ActiveSkillHit];
        if (null == currBulletConfig || null == currBulletConfig.CharacterEmitSfxName || currBulletConfig.CharacterEmitSfxName.IsEmpty()) return false;

        string sfxLookupKey = "ch-emit-" + currCharacterDownsync.JoinIndex.ToString();
        var sfxSourceHolder = cachedSfxNodes.PopAny(sfxLookupKey);
        if (null == sfxSourceHolder) {
            sfxSourceHolder = cachedSfxNodes.Pop();
        }

        if (null == sfxSourceHolder) {
            return false;
            // throw new ArgumentNullException(String.Format("No available sfxSourceHolder node for sfxLookupKey={0}", sfxLookupKey));
        }

        try {
            string clipName = currBulletConfig.CharacterEmitSfxName;
            if (null == clipName) {
                return false;
            }
            if (!sfxSourceHolder.audioClipDict.ContainsKey(clipName)) {
                return false;
            }

            newPosHolder.Set(wx, wy, distanceAttenuationZ);
            sfxSourceHolder.gameObject.transform.position = newPosHolder;
            sfxSourceHolder.audioSource.volume = calcSfxVolume(sfxSourceHolder, distanceAttenuationZ);
            sfxSourceHolder.audioSource.PlayOneShot(sfxSourceHolder.audioClipDict[clipName]);
            sfxSourceHolder.score = rdfId;
        } finally {
            cachedSfxNodes.Put(sfxLookupKey, sfxSourceHolder);
        }
        
        return true;
    }

    public bool playCharacterVfx(CharacterDownsync currCharacterDownsync, CharacterDownsync prevCharacterDownsync, CharacterConfig chConfig, float wx, float wy, int rdfId) {
        // Buff Vfx
        for (int i = 0; i < currCharacterDownsync.BuffList.Count; i++) {
            var buff = currCharacterDownsync.BuffList[i];
            if (TERMINATING_BUFF_SPECIES_ID == buff.SpeciesId) break;
            if (NO_VFX_ID == buff.BuffConfig.CharacterVfxSpeciesId) continue;
            if (BuffStockType.Timed == buff.BuffConfig.StockType && 0 >= buff.Stock) continue;
            var vfxConfig = vfxDict[buff.BuffConfig.CharacterVfxSpeciesId];
            if (!vfxConfig.OnCharacter) continue;
            var speciesKvPq = cachedVfxNodes[buff.BuffConfig.CharacterVfxSpeciesId];
            string lookupKey = "ch-buff-" + currCharacterDownsync.JoinIndex.ToString() + "-" + i;
            var vfxAnimHolder = speciesKvPq.PopAny(lookupKey);
            if (null == vfxAnimHolder) {
                vfxAnimHolder = speciesKvPq.Pop();
                //Debug.Log(String.Format("@rdfId={0} using a new vfxAnimHolder for rendering for lookupKey={1} at wpos=({2}, {3})", rdfId, lookupKey, currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY));
            } else {
                //Debug.Log(String.Format("@rdfId={0} using a cached vfxAnimHolder for rendering for lookupKey={1} at wpos=({2}, {3})", rdfId, lookupKey, currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY));
            }

            if (null == vfxAnimHolder) {
                throw new ArgumentNullException(String.Format("No available vfxAnimHolder node for lookupKey={0}", lookupKey));
            }

            bool isInitialFrame = (buff.Stock == buff.BuffConfig.Stock);

            if (vfxConfig.MotionType == VfxMotionType.Tracing) {
                newPosHolder.Set(wx, wy, vfxAnimHolder.gameObject.transform.position.z);
                vfxAnimHolder.gameObject.transform.position = newPosHolder;
            } else if (vfxConfig.MotionType == VfxMotionType.Dropped && isInitialFrame) {
                newPosHolder.Set(wx, wy, vfxAnimHolder.gameObject.transform.position.z);
                vfxAnimHolder.gameObject.transform.position = newPosHolder;
            }

            if (isInitialFrame) {
                vfxAnimHolder.attachedPs.Stop();
                vfxAnimHolder.attachedPs.Play();
            }
            vfxAnimHolder.score = rdfId;
            speciesKvPq.Put(lookupKey, vfxAnimHolder);
        }

        // Bullet Vfx
        {
            if (!skills.ContainsKey(currCharacterDownsync.ActiveSkillId)) return false;
            var currSkillConfig = skills[currCharacterDownsync.ActiveSkillId];
            if (NO_SKILL_HIT == currCharacterDownsync.ActiveSkillHit || currCharacterDownsync.ActiveSkillHit >= currSkillConfig.Hits.Count) {
                return false;
            }
            var currBulletConfig = currSkillConfig.Hits[currCharacterDownsync.ActiveSkillHit];
            if (null == currBulletConfig || NO_VFX_ID == currBulletConfig.ActiveVfxSpeciesId) {
                return false;
            }

            var vfxConfig = vfxDict[currBulletConfig.ActiveVfxSpeciesId];
            if (!vfxConfig.OnCharacter) return false;
            // The outer "if" is less costly than calculating viewport point.
            // if the current position is within camera FOV
            var speciesKvPq = cachedVfxNodes[currBulletConfig.ActiveVfxSpeciesId];
            string lookupKey = "ch-" + currCharacterDownsync.JoinIndex.ToString();
            var vfxAnimHolder = speciesKvPq.PopAny(lookupKey);
            if (null == vfxAnimHolder) {
                vfxAnimHolder = speciesKvPq.Pop();
                //Debug.Log(String.Format("@rdfId={0} using a new vfxAnimHolder for rendering for chJoinIndex={1} at wpos=({2}, {3})", rdfId, currCharacterDownsync.JoinIndex, currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY));
            } else {
                //Debug.Log(String.Format("@rdfId={0} using a cached vfxAnimHolder for rendering for chJoinIndex={1} at wpos=({2}, {3})", rdfId, currCharacterDownsync.JoinIndex, currCharacterDownsync.VirtualGridX, currCharacterDownsync.VirtualGridY));
            }

            if (null == vfxAnimHolder) {
                throw new ArgumentNullException(String.Format("No available vfxAnimHolder node for lookupKey={0}", lookupKey));
            }

            bool isInitialFrame = (currBulletConfig.StartupFrames == currCharacterDownsync.FramesInChState);
            // [WARNING] If any new Vfx couldn't be visible regardless of how big/small the z-index is set, review "Inspector > ParticleSystem > Renderer", make sure that "Sorting Layer Id" is set to a same value as that of a bullet!

            if (vfxConfig.MotionType == VfxMotionType.Tracing) {
                newPosHolder.Set(wx, wy, vfxAnimHolder.gameObject.transform.position.z);
                vfxAnimHolder.gameObject.transform.position = newPosHolder;
            } else if (vfxConfig.MotionType == VfxMotionType.Dropped && isInitialFrame) {
                if (VfxDashingActive.SpeciesId == currBulletConfig.ActiveVfxSpeciesId) {
                    // Special offset for Dashing
                    newPosHolder.Set(wx, wy - .5f * chConfig.DefaultSizeY * VIRTUAL_GRID_TO_COLLISION_SPACE_RATIO, vfxAnimHolder.gameObject.transform.position.z);
                } else {
                    newPosHolder.Set(wx, wy, vfxAnimHolder.gameObject.transform.position.z);
                }
                vfxAnimHolder.gameObject.transform.position = newPosHolder;
            }

            if (isInitialFrame) {
                // Regardless of "vfxConfig.DurationType" 
                if (0 > currCharacterDownsync.DirX) {
                    vfxAnimHolder.attachedPsr.flip = new Vector3(-1, 0);
                } else {
                    vfxAnimHolder.attachedPsr.flip = new Vector3(1, 0);
                }
                if (0 < currCharacterDownsync.ActiveSkillHit && !vfxAnimHolder.attachedPs.isPlaying) { 
                    // For a multi-hit bullet with vfx, we might need this to prevent duplicate triggers
                    vfxAnimHolder.attachedPs.Play();
                } else {
                    vfxAnimHolder.attachedPs.Stop();
                    vfxAnimHolder.attachedPs.Play();
                }
            }
            vfxAnimHolder.score = rdfId;
            speciesKvPq.Put(lookupKey, vfxAnimHolder);
        }
        
        return true;
    }

    public bool playBulletSfx(Bullet bullet, bool isExploding, float wx, float wy, int rdfId, float distanceAttenuationZ) {
        // Play "ActiveSfx" if configured
        bool shouldPlayActiveSfx = (0 < bullet.FramesInBlState && BulletState.Active == bullet.BlState && null != bullet.Config.ActiveSfxName);
        if (shouldPlayActiveSfx) {
            bool usingSameAudSrc = true;
            string atSfxLookupKey = "bl-at-" + bullet.BattleAttr.BulletLocalId.ToString();
            var atSfxSourceHolder = cachedSfxNodes.PopAny(atSfxLookupKey);
            if (null == atSfxSourceHolder) {
                atSfxSourceHolder = cachedSfxNodes.Pop();
                usingSameAudSrc = false;
            }

            if (null == atSfxSourceHolder) {
                return false;
                // throw new ArgumentNullException(String.Format("No available atSfxSourceHolder node for ftSfxLookupKey={0}", ftSfxLookupKey));
            }

            try {
                if (!atSfxSourceHolder.audioClipDict.ContainsKey(bullet.Config.ActiveSfxName)) {
                    return false;
                }

                float totAttZ = distanceAttenuationZ + footstepAttenuationZ; // Use footstep built-in attenuation for now
                newPosHolder.Set(wx, wy, totAttZ);
                atSfxSourceHolder.gameObject.transform.position = newPosHolder;
                if (!usingSameAudSrc || !atSfxSourceHolder.audioSource.isPlaying) {
                    atSfxSourceHolder.audioSource.volume = calcSfxVolume(atSfxSourceHolder, totAttZ);
                    atSfxSourceHolder.audioSource.PlayOneShot(atSfxSourceHolder.audioClipDict[bullet.Config.ActiveSfxName]);
                }
                atSfxSourceHolder.score = rdfId;
            } finally {
                cachedSfxNodes.Put(atSfxLookupKey, atSfxSourceHolder);
            }
        }

        // Play initla sfx for state
        bool isInitialFrame = (0 == bullet.FramesInBlState && (BulletState.Active != bullet.BlState || (BulletState.Active == bullet.BlState && 0 < bullet.BattleAttr.ActiveSkillHit)));
        if (!isInitialFrame) {
            return false;
        }
        string sfxLookupKey = "bl-" + bullet.BattleAttr.BulletLocalId.ToString();
        var sfxSourceHolder = cachedSfxNodes.PopAny(sfxLookupKey);
        if (null == sfxSourceHolder) {
            sfxSourceHolder = cachedSfxNodes.Pop();
        }

        if (null == sfxSourceHolder) {
            return false;
            // throw new ArgumentNullException(String.Format("No available sfxSourceHolder node for sfxLookupKey={0}", sfxLookupKey));
        }

        try {
            string clipName = isExploding ? bullet.Config.ExplosionSfxName : bullet.Config.FireballEmitSfxName;
            if (null == clipName) {
                return false;
            }
            if (!sfxSourceHolder.audioClipDict.ContainsKey(clipName)) {
                return false;
            }

            newPosHolder.Set(wx, wy, distanceAttenuationZ);
            sfxSourceHolder.gameObject.transform.position = newPosHolder;
            sfxSourceHolder.audioSource.volume = calcSfxVolume(sfxSourceHolder, distanceAttenuationZ);
            sfxSourceHolder.audioSource.PlayOneShot(sfxSourceHolder.audioClipDict[clipName]);
            sfxSourceHolder.score = rdfId;
        } finally {
            cachedSfxNodes.Put(sfxLookupKey, sfxSourceHolder);
        }
        
           
        return true;
    }

    public bool playBulletVfx(Bullet bullet, bool isExploding, float wx, float wy, int rdfId) {
        int vfxSpeciesId = isExploding ? bullet.Config.ExplosionVfxSpeciesId : bullet.Config.ActiveVfxSpeciesId;
        if (!isExploding && !IsBulletActive(bullet, rdfId)) return false;
        newPosHolder.Set(wx, wy, fireballZ);
        if (NO_VFX_ID == vfxSpeciesId || !isGameObjPositionWithinCamera(newPosHolder)) return false;
        var vfxConfig = vfxDict[vfxSpeciesId];
        if (!vfxConfig.OnBullet) return false;
        var speciesKvPq = cachedVfxNodes[vfxSpeciesId];
        string vfxLookupKey = "bl-" + bullet.BattleAttr.BulletLocalId.ToString();
        var vfxAnimHolder = speciesKvPq.PopAny(vfxLookupKey);
        if (null == vfxAnimHolder) {
            vfxAnimHolder = speciesKvPq.Pop();
            //Debug.Log(String.Format("@rdfId={0} using a new vfxAnimHolder for rendering for bulletLocalId={1} at wpos=({2}, {3})", rdfId, bullet.BattleAttr.BulletLocalId, bullet.VirtualGridX, bullet.VirtualGridY));
        } else {
            //Debug.Log(String.Format("@rdfId={0} using a cached vfxAnimHolder for rendering for bulletLocalId={1} at wpos=({2}, {3})", rdfId, bullet.BattleAttr.BulletLocalId, bullet.VirtualGridX, bullet.VirtualGridY));
        }

        if (null == vfxAnimHolder) {
            throw new ArgumentNullException(String.Format("No available vfxAnimHolder node for vfxLookupKey={0}", vfxLookupKey));
        }
        // [WARNING] If any new Vfx couldn't be visible regardless of how big/small the z-index is set, review "Inspector > ParticleSystem > Renderer", make sure that "Sorting Layer Id" is set to a same value as that of a bullet! 

        bool isInitialFrame = (0 == bullet.FramesInBlState);
        if (vfxConfig.MotionType == VfxMotionType.Tracing) {
            newPosHolder.Set(wx, wy, vfxAnimHolder.gameObject.transform.position.z);
            vfxAnimHolder.gameObject.transform.position = newPosHolder;
        } else if (vfxConfig.MotionType == VfxMotionType.Dropped && isInitialFrame) {
            newPosHolder.Set(wx, wy, vfxAnimHolder.gameObject.transform.position.z);
            vfxAnimHolder.gameObject.transform.position = newPosHolder;
        }

        if (isInitialFrame) {
            // Regardless of "vfxConfig.DurationType" 
            vfxAnimHolder.attachedPs.Play();
        }

        vfxAnimHolder.score = rdfId;
        speciesKvPq.Put(vfxLookupKey, vfxAnimHolder);
        return true;
    }

    public float calcSfxVolume(SFXSource sfxSource, float totAttZ) {
        if (totAttZ <= 0) return 1f;
        if (totAttZ >= sfxSource.maxDistanceInWorld) return 0f;
        return (float)Math.Pow((double)12f, (double)(-totAttZ/sfxSource.maxDistanceInWorld));
    }
    public string calcFootStepSfxName(CharacterDownsync currCharacterDownsync) {
        // TODO: Record the contacted barrier material ID in "CharacterDownsync" to achieve more granular footstep sound derivation!  
        return "FootStep1";
    }
}
